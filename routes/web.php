<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('home', 'AccountController@home')->name('home');
Route::get('account', 'AccountController@index')->name('account');


// Route::resource('profile','MyProfileController');
Route::get('jsonMaterials', 'ServiceController@jsonMaterials')->name('jsonMaterials');
Route::get('jsonExploreService', 'ServiceController@jsonExploreService')->name('jsonExploreService');
Route::get('jsonCountService', 'ServiceController@jsonCountService')->name('jsonCountService');
Route::get('jsonMerchantInfo', 'ServiceController@jsonMerchantInfo')->name('jsonMerchantInfo');
Route::get('jsonLocation', 'AccountController@jsonLocation')->name('jsonLocation');
Route::get('jsonCountOrder', 'OrderController@jsonCountOrder')->name('jsonCountOrder');


Route::get('order', 'OrderController@index')->name('order');
Route::get('order/{id}', 'OrderController@show')->name('show_order');
Route::get('create_order/{id}', 'OrderController@create')->name('create_order');
Route::get('customer_location/{id}', 'OrderController@customerLocation')->name('customer_location');

Route::get('explore', 'HomeController@explore')->name('explore');
Route::post('store_order', 'OrderController@store')->name('store_order');

Route::get('my_services', 'ServiceController@index')->name('my_services');
Route::get('create_service', 'ServiceController@create')->name('create_service');
Route::post('store_service', 'ServiceController@store')->name('store_service');
Route::get('service/{id}', 'ServiceController@show')->name('show_service');
Route::get('edit_service/{id}', 'ServiceController@edit')->name('edit_service');

Route::get('testing', 'HomeController@testing')->name('testing');

// ACCOUNT - prefix group for account
Route::group(['prefix' => 'account/'], function()
{
    Route::get('', 'AccountController@index')->name('account');
    Route::get('edit', 'AccountController@edit')->name('account_edit');
    Route::put('update', 'AccountController@update')->name('account_update');
});

Route::post('store_status', 'StatusController@store')->name('store_status');

Route::get('merchant/{id}', 'AccountController@show')->name('show_merchant');
Route::get('test', 'AccountController@test')->name('test');



// MERCHANT - prefix group for user
// Route::group(['prefix' => 'merchant/'], function()
// {
//     // prefix group for merchant (account)
//     Route::group(['prefix' => 'account/'], function()
//     {
//         Route::get('', 'AccountController@index')->name('merchant_account');
//         // Route::get('edit', 'AccountController@edit')->name('merchant_accountEdit');
//         Route::put('update', 'AccountController@update')->name('merchant_accountUpdate');
//         Route::get('change_password', 'AccountController@changePassword')->name('merchant_changePassword');
//         Route::post('update_password', 'AccountController@updatePassword')->name('merchant_updatePassword');
//     });

//     // prefix group for merchant (service)
//     Route::group(['prefix' => 'service/'], function()
//     {
//         // Route::get('my_services', 'ServiceController@index')->name('my_services');
//         Route::get('create_service', 'ServiceController@create')->name('create_service');
//         Route::post('store_service', 'ServiceController@store')->name('store_service');
//         Route::get('show_service/{id}', 'ServiceController@show')->name('show_service');
//         Route::get('edit_service/{id}', 'ServiceController@edit')->name('edit_service');
//     });
    
//     // prefix group for merchant (order)
//     Route::group(['prefix' => 'order/'], function()
//     {
//         Route::post('', 'StatusController@store')->name('store_status');
//     });

// });





// // CUSTOMER - prefix group for user
Route::group(['prefix' => 'customer/'], function()
{    
//     // Route::get('explore', 'HomeController@explore')->name('explore');
//     Route::get('service_detail/{id}', 'ServiceController@show')->name('show_service');
//     Route::post('store_order', 'OrderController@store')->name('store_order');
    // Route::get('/merchant', 'ServiceController@show')->name('merchant');
//     Route::get('create_order/{id}', 'OrderController@create')->name('create_order');
//     Route::get('order/{id}', 'OrderController@show')->name('show_order_customer');

});


