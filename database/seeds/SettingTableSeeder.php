<?php

use Illuminate\Database\Seeder;
use App\Setting;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $serviceItems = [
          //key array huruf kecil dan tanpa spasi untuk dimasukkan ke database
          'blueprint' => 'Blue Print',
          'brosursatusisi' => 'Brosur 1 Sisi',
          'brosurduasisi' => 'Brosur 2 Sisi',
          'gambarteknik' => 'Gambar Teknik',
          'kalenderdinding' => 'Kalender Dinding',
          'kalendermeja' => 'Kalender Meja',
          'namecardsatusisi' => 'Name Card 1 Sisi',
          'namecardduasisi' => 'Name Card 2 Sisi',
          'poster' => 'Poster',
          'rollupbanner' => 'Roll Up Banner',
          'sertifikat' => 'Sertifikat',
          'tintprint' => 'Tint Print',
          'xbanner' => 'X-Banner',
          'minixbanner' => 'Mini X-Banner',
          'ybanner' => 'Y-Banner',
          // 'laserprintandphotocopy' => 'Laser Print & Photocopy',
          // 'tintprintandphotocopy' => 'Tint Print & Photocopy'
          // 'spanduk' => 'Spanduk',
          // 'banner' => 'Banner',
        ];
        
        $materialNames = [
          'artcarton' => 'Art Carton',
          'artpaper' => 'Art Paper',
          'aster' => 'Aster',
          'bannermaterial' => 'Banner Material',
          'boardpaper' => 'Board Paper',
          'concorde' => 'Concorde',
          'duplex' => 'Duplex',
          'fancy' => 'Fancy',
          'hammer' => 'Hammer',
          'hvs' => 'HVS',
          'ivory' => 'Ivory',
          'jasmine' => 'Jasmine',
          'linen' => 'Linen',
          'manila' => 'Manila',
          'samson' => 'Samson',
        ];
        
        $materialSizes = [
          'a0' => 'A0 (84.1 x 118.9 cm)',
          'a1' => 'A1 (59.4 x 84.1 cm)',
          'a2' => 'A2 (42 x 59.4 cm)',
          'a3' => 'A3 (29.7 x 42 cm)',
          'a3plus' => 'A3+ (32.9 x 48.3 cm)',
          'a4' => 'A4 (21 x 29.7 cm)',
          
          'b0' => 'B0 (100 x 141.4 cm)',
          'b1' => 'B1 (70.7 x 100 cm)',
          'b2' => 'B2 (50 x 70.7 cm)',
          'b3' => 'B3 (35.3 x 50 cm)',
          'b4' => 'B4 (25 x 35.3 cm)',
          
          'c0' => 'C0 (91.7 x 129.7 cm)',
          'c1' => 'C1 (64.8 x 91.7 cm)',
          'c2' => 'C2 (45.8 x 64.8 cm)',
          'c3' => 'C3 (32.4 x 45.8 cm)',
          'c4' => 'C4 (22.9 x 32.4 cm)',
          
          'xb60160' => 'X-Banner Size (60 x 160 cm)',
          'mxb60160' => 'Mini X-Banner Size (24 x 38 cm)',
          'yb80200' => 'Y-Banner Size (60 x 160 cm)',
        ];

        $kabupaten = [
          'bantul' => 'Bantul',
          'gunungkidul' => 'Gunung Kidul',
          'kotayogyakarta' => 'Kota Yogyakarta',
          'kulonprogo' => 'Kulon Progo',
          'sleman' => 'Sleman'
        ];

        $bank_names = [
          'mandiri' => 'Mandiri',
          'bni' => 'BNI',
          'bri' => 'BRI',
          'bca' => 'BCA'
        ];

        $items = [
          [
            'setting_key' => 'service',
            'setting_value' => serialize($serviceItems)
          ],
          [
            'setting_key' => 'matNames',
            'setting_value' => serialize($materialNames)
          ],
          [
            'setting_key' => 'matSizes',
            'setting_value' => serialize($materialSizes)
          ],
          [
            'setting_key' => 'kabupaten',
            'setting_value' => serialize($kabupaten)
          ],
          [
            'setting_key' => 'bank',
            'setting_value' => serialize($bank_names)
          ],
        ];


        foreach ($items as $key => $item) {
          Setting::create($item);
        }
    }
}
