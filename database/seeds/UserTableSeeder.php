<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
          [
            'name' => 'Klik Document Solution',
            'email' => 'klikdocumentsolution@gmail.com',
            'phone_number' => '81244324423',
            'bank' => 'mandiri',
            'account_number' => '9000008289299',
            'account_name' => 'Adi Budiono',
            'password' => 'JvcG:(?om8',
            'lat' => '-7.775704',
            'long' => '110.388843',
            'role' => 'merchant'
          ],
          [
            'name' => 'Mangrove Digital Printing',
            'email' => 'mangrove.dp@gmail.com',
            'phone_number' => '82255006699',
            'bank' => 'mandiri',
            'account_number' => '9000005129887',
            'account_name' => 'Cahyono Dedi',
            'password' => 'ST6E@s8jH(',
            'lat' => '-7.768872',
            'long' => '110.390228',
            'role' => 'merchant'
          ],
          [
            'name' => 'Benny Vahlevi',
            'email' => 'benny.vahlevi@gmail.com',
            'phone_number' => '81311144479',
            'bank' => 'mandiri',
            'account_number' => '9000005436432',
            'account_name' => 'Benny Vahlevi',
            'password' => '[cSt92/5Jj',
            'lat' => '-7.748547',
            'long' => '110.401021',
            'role' => 'customer'
          ],
          // [
          //   'name' => 'XTreme Kaliurang',
          //   'email' => 'xtremekaliurang@gmail.com',
          //   'phone_number' => '81111111113',
          //   'password' => '121212',
          //   'role' => 'merchant'
          // ],
          // [
          //   'name' => 'Ortindo Jogja',
          //   'email' => 'ortindo.official@gmail.com',
          //   'phone_number' => '81111111114',
          //   'password' => '121212',
          //   'role' => 'merchant'
          // ],
          // [
          //   'name' => 'Kedai Grafis',
          //   'email' => 'kedaigrafis@gmail.com',
          //   'phone_number' => '81111111115',
          //   'password' => '121212',
          //   'role' => 'merchant'
          // ],
          // [
          //   'name' => 'Enggal Maju',
          //   'email' => 'enggalmaju@gmail.com',
          //   'phone_number' => '81111111116',
          //   'password' => '121212',
          //   'role' => 'merchant'
          // ],
          // [
          //   'name' => 'Amanda Printing',
          //   'email' => 'Amandaprinting@gmail.com',
          //   'phone_number' => '81111111117',
          //   'password' => '121212',
          //   'role' => 'merchant'
          // ],
          // [
          //   'name' => 'Rejeki Percetakan',
          //   'email' => 'rejekipercetakan@gmail.com',
          //   'phone_number' => '81111111118',
          //   'password' => '121212',
          //   'role' => 'merchant'
          // ],
          // [
          //   'name' => 'Mediapro Creasindo',
          //   'email' => 'mediaprocreasindo@gmail.com',
          //   'phone_number' => '81111111119',
          //   'password' => '121212',
          //   'role' => 'merchant'
          // ],
          // [
          //   'name' => 'Talang Grafika',
          //   'email' => 'talanggrafika@gmail.com',
          //   'phone_number' => '81111111110',
          //   'password' => '121212',
          //   'role' => 'merchant'
          // ],
          // [
          //   'name' => 'Admin',
          //   'email' => 'admin@gmail.com',
          //   'phone_number' => '81324354657',
          //   'password' => '121212',
          //   'role' => 'admin'
          // ]
        ];

        foreach ($users as $key => $user) {
          // code...
          $dataUser = User::create([
            'name' => $user['name'],
            'email' => $user['email'],
            'phone_number' => $user['phone_number'],
            'password' => Hash::make($user['password']),
            'bank' => $user['bank'],
            'account_number' => $user['account_number'],
            'account_name' => $user['account_name'],
            'lat' => $user['lat'],
            'long' => $user['long']
          ]);

          $dataUser->assignRole($user['role']);
        }
    }
}
