<?php

use Illuminate\Database\Seeder;
use App\Service;
use Illuminate\Support\Facades\Hash;

class ServiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $services = [
          // [
          //   'user_id' => '1',
          //   'name' => 'banner',
          //   'description' => 'test',
          // ],
          // [
          //   'user_id' => '2',
          //   'name' => 'banner',
          //   'description' => 'test',
          // ],
          // [
          //   'user_id' => '3',
          //   'name' => 'banner',
          //   'description' => 'test',
          // ],
          // [
          //   'user_id' => '4',
          //   'name' => 'banner',
          //   'description' => 'test',
          // ],
          // [
          //   'user_id' => '5',
          //   'name' => 'banner',
          //   'description' => 'test',
          // ],
          // [
          //   'user_id' => '6',
          //   'name' => 'banner',
          //   'description' => 'test',
          // ],
          // [
          //   'user_id' => '7',
          //   'name' => 'banner',
          //   'description' => 'test',
          // ],
          // [
          //   'user_id' => '8',
          //   'name' => 'banner',
          //   'description' => 'test',
          // ],
          // [
          //   'user_id' => '9',
          //   'name' => 'banner',
          //   'description' => 'test',
          // ],
          // [
          //   'user_id' => '10',
          //   'name' => 'banner',
          //   'description' => 'test',
          // ],
          // [
          //   'user_id' => '1',
          //   'name' => 'poster',
          //   'description' => 'test',
          // ],
          // [
          //   'user_id' => '2',
          //   'name' => 'poster',
          //   'description' => 'test',
          // ],
          // [
          //   'user_id' => '3',
          //   'name' => 'poster',
          //   'description' => 'test',
          // ],
          // [
          //   'user_id' => '4',
          //   'name' => 'poster',
          //   'description' => 'test',
          // ],
          // [
          //   'user_id' => '5',
          //   'name' => 'poster',
          //   'description' => 'test',
          // ],
          // [
          //   'user_id' => '6',
          //   'name' => 'poster',
          //   'description' => 'test',
          // ],
          // [
          //   'user_id' => '7',
          //   'name' => 'poster',
          //   'description' => 'test',
          // ],
          // [
          //   'user_id' => '8',
          //   'name' => 'poster',
          //   'description' => 'test',
          // ],
          // [
          //   'user_id' => '9',
          //   'name' => 'poster',
          //   'description' => 'test',
          // ],
          // [
          //   'user_id' => '10',
          //   'name' => 'poster',
          //   'description' => 'test',
          // ],
          // [
          //   'user_id' => '1',
          //   'name' => 'namecard',
          //   'description' => 'test',
          // ],
          // [
          //   'user_id' => '2',
          //   'name' => 'namecard',
          //   'description' => 'test',
          // ],
          // [
          //   'user_id' => '3',
          //   'name' => 'namecard',
          //   'description' => 'test',
          // ],
          // [
          //   'user_id' => '4',
          //   'name' => 'namecard',
          //   'description' => 'test',
          // ],
          // [
          //   'user_id' => '5',
          //   'name' => 'namecard',
          //   'description' => 'test',
          // ],
          // [
          //   'user_id' => '6',
          //   'name' => 'namecard',
          //   'description' => 'test',
          // ],
          // [
          //   'user_id' => '7',
          //   'name' => 'namecard',
          //   'description' => 'test',
          // ],
          // [
          //   'user_id' => '8',
          //   'name' => 'namecard',
          //   'description' => 'test',
          // ],
          // [
          //   'user_id' => '9',
          //   'name' => 'namecard',
          //   'description' => 'test',
          // ],
          // [
          //   'user_id' => '10',
          //   'name' => 'namecard',
          //   'description' => 'test',
          // ],
        ];

        foreach ($services as $key => $service) {
          // code...
          $dataService = Service::create([
            'user_id' => $service['user_id'],
            'name' => $service['name'],
            'description' => $service['description'],
            // 'image' => $service['image']
          ]);
        }
    }
}
