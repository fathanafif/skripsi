<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'order_code', 'customer_id', 'service_id', 'order_note', 'material_id', 'copies', 'file', 'total_cost', 'pick_up'
    ];

    public function customer()
    {
        return $this->belongsTo('App\User', 'customer_id'); //isi di dalam kurung modelnya dan unsignednya(migration file)
    }
    
    public function service()
    {
        return $this->belongsTo('App\Service', 'service_id');
    }
    
    public function material()
    {
        return $this->belongsTo('App\Material', 'material_id');
    }
    
    public function statuses()
    {
        return $this->hasMany('App\Status', 'order_id');
    }

    public function lastStatus()
    {
        return $this->hasOne('App\Status', 'order_id')->latest('id');
        // return $this->hasOne('App\Status', 'order_id')->latest();
    }

    public function getThumbnailAttribute()
    {
        $thumbnail = null;
        if (substr($this->file, -3) == ".ai") {
            $thumbnail = "illustrator.svg";
        } elseif (substr($this->file, -4) == ".psd") {
            $thumbnail = "psd.svg";
        } elseif (substr($this->file, -4) == ".cdr") {
            $thumbnail = "corelDraw.png";
        } elseif (substr($this->file, -4) == ".pdf") {
            $thumbnail = "pdf.svg";
        } else {
            $thumbnail = "blankHead.png";
        }
        return asset('assets/'.$thumbnail);
    }
}