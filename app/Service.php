<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected static $settings = [];

    protected $fillable = [
        'user_id', 'name', 'estimated_time', 'description', 'image'
    ];

    protected $appends = [
        'real_name', // ini itu getRealNameAttribute()
    ];

    public function user()
    {
        //isi di dalam kurung modelnya dan unsignednya(migration file)
        return $this->belongsTo('App\User', 'user_id');
    }
    
    public function materials()
    {
        return $this->hasMany('App\Material', 'service_id');
    }
    public function orders()
    {
        return $this->hasMany('App\Order', 'service_id');
    }

    public function getRealNameAttribute()
    {
        return $this->getSettingValue('service')[$this->name];
    }

    public function getSettingValue($key)
    {
        if (isset(static::$settings[$key])) {
            $setting = static::$settings[$key];
        } else {
            $setting = static::$settings[$key] = Setting::where('setting_key', $key)->first();
        }

        return unserialize($setting->setting_value);
    }
}