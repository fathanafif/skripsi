<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasRoles, Notifiable;

    protected static $settings = [];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','phone_number',
    ];

    protected $appends = [
        'real_bank_name', // ini itu getRealNameAttribute()
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function services()
    {
        return $this->hasMany('App\Service', 'user_id');
    }

    public function orders()
    {
        return $this->hasManyThrough(
            'App\Order',
            'App\Service',
            'user_id',
            'service_id',
            'id',
            'id'
        );
    }

    public function getRealBankNameAttribute()
    {
        $setting = $this->getSettingValue('bank');
        if (isset($setting[$this->bank])) {
            return $setting[$this->bank];
        }
        return null;
    }

    public function getSettingValue($key)
    {
        if (isset(static::$settings[$key])) {
            $setting = static::$settings[$key];
        } else {
            $setting = static::$settings[$key] = Setting::where('setting_key', $key)->first();
        }

        return unserialize($setting->setting_value);
    }
    
}
