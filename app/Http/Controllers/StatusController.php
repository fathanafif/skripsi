<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\User;
use App\Setting;
use App\Service;
use App\Material;
use App\Status;
use App\Order;
use Response;
use View;

class StatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $me = auth()->user();
        if ($me->hasAnyRole('merchant')) {
            $request->validate([
                'order_id' => 'required|exists:orders,id'
            ]);
            
            if ($request->order_message !== null && $request->order_id !== null) {
                $validation = $request->validate([
                    'order_message' => 'required|max:100',
                    'order_id' => 'required'
                ]);
                $status = Status::create([
                    'order_id' => $request->order_id,
                    'title' => 'Confirmed by merchant',
                    'message' => $request->orderMessage,
                ]);
                return redirect()->route('show_order', $request->order_id);

            } elseif ($request->decision == "Decline") {
                $validation = $request->validate([
                    'declineMessage' => 'required|max: 100',
                    'order_id' => 'required'
                ]);
                $status = Status::create([
                    'order_id' => $request->order_id,
                    'title' => 'Declined',
                    'message' => $request->declineMessage,
                ]);
                return redirect()->route('show_order', $request->order_id);

            } elseif ($request->proofing_photo !== null) {
                $request->validate([
                    // 'proofing_photo' => 'required',
                    'order_id' => 'required'
                ]);
                $filePath = $request->file('proofing_photo')->store('uploads/proofing', 'public');
                $status = Status::create([
                    'order_id' => $request->order_id,
                    'title' => "Proofing",
                    'proofing' => $filePath,
                ]);
                return redirect()->route('show_order', $request->order_id);

            } elseif ($request->order_id !== null && $request->payment_verification == "1") {
                $status = Status::create([
                    'order_id' => $request->order_id,
                    'title' => "Slip is verified",
                ]);
                return redirect()->route('show_order', $request->order_id);

            } elseif ($request->order_id !== null && $request->finished_order == "1") {
                $status = Status::create([
                    'order_id' => $request->order_id,
                    'title' => "Order is ready for pick up",
                ]);
                return redirect()->route('show_order', $request->order_id);
            }  else {
                dd("Error");
                return redirect()->route('order');
            }

        } elseif ($me->hasAnyRole('customer')) {

            if ($request->order_id !== null && $request->proofingConfirmation == "1") {
                $status = Status::create([
                    'order_id' => $request->order_id,
                    'title' => "Confirmed by customer",
                ]);
                return redirect()->route('show_order', $request->order_id);

            } elseif ($request->payment == "Upload Payment Slip") {
                $status = Status::create([
                    'order_id' => $request->order_id,
                    'title' => 'Waiting for Payment Slip',
                ]);
                return redirect()->route('show_order', $request->order_id);

            } elseif ($request->order_id !== null || $request->payment_slip !== null) {
                $validation = $request->validate([
                    // 'paymentSlip' => 'required|image|mimes:jpeg,jpg,png|max: 2048',
                ]);
                $filePath = $request->file('payment_slip')->store('uploads/paymentSlip', 'public');
                $status = Status::create([
                    'order_id' => $request->order_id,
                    'payment' => $filePath,
                    'title' => 'Transfered',
                ]);
                return redirect()->route('show_order', $request->order_id);

            } else {
                $status = Status::create([
                    'order_id' => $request->order_id,
                    'title' => 'Declined',
                ]);
                return redirect()->route('customer_order');
            }
        } else {
            return redirect('/');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
