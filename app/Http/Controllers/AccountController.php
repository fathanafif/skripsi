<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use App\User;
use App\Setting;

class AccountController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */

  public function __construct()
  {
      $this->middleware('auth');
  }

  public function index()
  {
    $me = auth()->user();
    $bank = $me->bank;
    $setBank = unserialize(Setting::where('setting_key', 'bank')->first()->setting_value);
    if ($me->hasAnyRole('merchant')) {
      // $user = User::where('id', $me->id)->first();
      // return response()->json($user);
      return view('users/merchant/account/account', compact('me', 'setBank'));
    } elseif ($me->hasAnyRole('customer')) {
      // $user = User::where('id', $me->id)->first();
      // return response()->json($user);
      return view('users/customer/account/account', compact('me', 'setBank'));
    } else {
      return view('welcome');
    }
  }
  
  public function home()
  {
    $me = auth()->user();
    if ($me->hasAnyRole('merchant')) {
      $users = User::all();
      return redirect()->route('my_services');
      // return view('my_service', compact('users'));
    } elseif ($me->hasAnyRole('customer')) {
      $users = User::all();
      return redirect()->route('explore');
    } else {
      return view('welcome');
    }
  }
  
  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    //
  }
  
  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    //
  }
  
  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    $merchant = User::where('id', $id)->with('services')->first();
    return view('users/customer/show_merchant', compact('merchant'));
  }

  
  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit()
  {
    $me = auth()->user();
    $bank = $me->bank;
    $setBank = unserialize(Setting::where('setting_key', 'bank')->first()->setting_value);
    if ($me->hasAnyRole('merchant')) {
      return view('users/merchant/account/edit-account', compact('me', 'setBank'));
    } elseif ($me->hasAnyRole('customer')) {
      // return response()->json($me);
      return view('users/customer/account/edit-account', compact('me', 'setBank'));
    } else {
      return view('welcome');
    }
  }
  
  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request)
  {
    //daripada ngambil id lagi, lebih baik sekaligus ambil dari id user yang login
    $account = auth()->user();
    $this->validate($request, [
      'name' => 'required|max:30',
      'email' => [
        'required',
        'email',
        Rule::unique('users')->ignore($account->id),
      ],
      'phone_number' => [
        'required',
        Rule::unique('users')->ignore($account->id),
      ],
      'bank' => 'required',
      'account_name' => 'required|max:20',
      'account_number' => [
        'required',
        Rule::unique('users', 'account_number')->ignore($account->id),
      ],
      ]);
      if ($request->hasFile('profile_photo')) {
        $oldImage = $account->profile_photo;
        if ($oldImage != '' && Storage::exists('public/'. $oldImage)) {
          Storage::delete('public/'. $oldImage);
        } 
        $imagePath = $request->file('profile_photo')->store('uploads/account', 'public');
        $account->profile_photo = $imagePath;
      }
      $account->name = $request['name'];
      $account->email = $request['email'];
      $account->phone_number = $request['phone_number'];
      $account->bank = $request['bank'];
      $account->account_name = $request['account_name'];
      $account->account_number = $request['account_number'];
      $account->lat = $request['lat'];
      $account->long = $request['long'];
      
      $account->update();
      return redirect()->route('account');
    }
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
      //
    }
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function changePassword()
    {
      $me = auth()->user();
      return view('users/merchant/account/change-password', compact('me'));
    }
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function updatePassword()
    {
      $profile = User::find(auth()->user()->id);
      dd($profile);
      
    }

    public function test()
    {
      return view('testing');
    }
  }
  