<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\User;
use App\Setting;
use App\Service;
use App\Material;
use Response;
use View;

class ServiceController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function jsonMaterials()
    {
        $materialNames = Setting::where('setting_key','matNames')->first();
        $materialNames = unserialize($materialNames->setting_value);
        $materialSizes = Setting::where('setting_key','matSizes')->first();
        $materialSizes = unserialize($materialSizes->setting_value);
        return response::json(array(
            'material_names' => $materialNames,
            'material_sizes' => $materialSizes,
        ));
    }
    
    public function jsonExploreService()
    {
        $exploreServices = Service::all();
        $exploreServices->load('materials');
        $exploreServices = $exploreServices->map(function ($item, $key) {
            $item->merchant = $item->user->name;
            $item->link_merchant = route('show_merchant', $item->user->id);
            $item->create_order = route('create_order', $item->id);
            return $item;
        });
        return response()->json($exploreServices);
        
    }

    public function jsonMerchantInfo(Request $request)
    {
        // $service = Service::find($request['service_id']);
        return response()->json($request->all());
    }

    public function jsonCountService()
    {
        $me = Auth()->user();
        $countService = Service::where('user_id', $me->id)->count();
        return response()->json($countService);
    }
    
    public function index()
    {
        $me = auth()->user();
        if ($me->hasAnyRole('merchant')) {
            //manggil kolom setting_key dengan value service
            $services = Service::where('user_id', $me->id)->with('materials')->latest()->paginate(16);
            $setService = unserialize(Setting::where('setting_key', 'service')->first()->setting_value);
            return view('users/merchant/service/my-service', compact('services', 'setService'));
        } elseif ($me->hasAnyRole('customer')) {
            return redirect()->route('explore');
        } else {
            return redirect('/');
        }
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    
    public function create()
    {
        $me = auth()->user();
        if ($me->hasAnyRole('merchant')) {
            if ($me->bank == null && $me->accountNumber == null) {
                return redirect()->route('account_edit')->withErrors('Lengkapi informasi account terlebih dahulu.');
            } else {
                $itemsService = Setting::where('setting_key','service')->first();
                $materialNames = Setting::where('setting_key','matNames')->first();
                $materialSizes = Setting::where('setting_key','matSizes')->first();
                $itemsService = unserialize($itemsService->setting_value);
                $materialNames = unserialize($materialNames->setting_value);
                $materialSizes = unserialize($materialSizes->setting_value);
                $verifData = $me->toArray();
                $itemsForDelete = [
                    'email_verified_at',
                    'created_at',
                    'updated_at'
                ];
                
                foreach ($itemsForDelete as $value) {
                    unset($verifData [$value]);
                }
                return view('users/merchant/service/create-service', compact('itemsService', 'materialNames', 'materialSizes'));
            }
            
        } else {
            return redirect('/');
        }
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $me = auth()->user();
        $validation = $request->validate([
            'estimatedTime' => 'required|numeric|min:1|max:48',
            'description' => 'required|max:500',
            'materialName' => 'required',
            'gramatur' => 'required',
            'materialSize' => 'required',
            'price' => 'required',
        ]);

        $checkService = Service::where('user_id',$me->id)->pluck('name','id')->toArray();
        if (in_array($request['serviceName'], $checkService))
        {
            return redirect()->route('create_service')->withErrors(['Kamu sudah memiliki service cetak ' . $request['serviceName'] . '.']);
        } else {
            if (request()->has('image')) {
                $imagePath = $request->file('image')->store('uploads', 'public');
                $service = Service::create([
                    'user_id' => $me->id,
                    'name' => $request['serviceName'],
                    'estimated_time' => $request['estimatedTime'],
                    'description' => $request['description'],
                    'image' => $imagePath,
                ]);
            } else {
                $service = Service::create([
                    'user_id' => $me->id,
                    'name' => $request['serviceName'],
                    'estimated_time' => $request['estimatedTime'],
                    'description' => $request['description'],
                ]); 
            }
            foreach($request['materialName'] as $key => $val)
            {       
                $material = Material::create([
                    'service_id' => $service->id,
                    'material_name' => $val,
                    //ambil request pada array materialSize berdasarkan keynya
                    'gramatur' => $request['gramatur'][$key],
                    'material_size' => $request['materialSize'][$key],
                    'price' => $request['price'][$key]
                ]);
            }
            
            return redirect()->route('my_services');
        }        
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $me = auth()->user();
        if ($me->hasAnyRole('merchant')) {
            $service = Service::with('materials', 'user')->find($id);
            return view('users/merchant/service/show-service', compact('service'));
        } elseif ($me->hasAnyRole('customer')) {
            $service = Service::with('materials', 'user')->find($id);
            // return response()->json($service);
            return view('users/customer/show-service', compact('service'));
        } else {
            return view('welcome');
        }
        
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        $me = auth()->user();
        $service = Service::find($id);
        $itemsService = Setting::where('setting_key','service')->first();
        $materialNames = Setting::where('setting_key','matNames')->first();
        $materialSizes = Setting::where('setting_key','matSizes')->first();
        $itemsService = unserialize($itemsService->setting_value);
        $materialNames = unserialize($materialNames->setting_value);
        $materialSizes = unserialize($materialSizes->setting_value);
        $verifData = $me->toArray();
        $itemsForDelete = [
            'email_verified_at',
            'created_at',
            'updated_at'
        ];    
        return view('users/merchant/service/edit-service', compact('itemsService', 'materialNames', 'materialSizes', 'service'));
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        //
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        //
    }

    
}
