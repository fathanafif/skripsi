<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\User;
use App\Setting;
use App\Service;
use App\Material;
use App\Status;
use App\Order;
use Response;
use View;
use App\Rules\WithoutOther;

class OrderController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function __construct()
    {
        $this->middleware('auth');
        // $users = User::all();
    }
    
    public function jsonCountOrder()
    {
        // $orders = $me->orders()->joinSub(function($query) {
        //     $query->from('statuses')
        //         ->selectRaw('id, service_id, max(id) as max_id');
        // }, 'sub', function ($query) {
        //     $query->on('sub.order_id', '=', 'services.id');
        // });
        // dd($orders->toSql());
        
        $me = Auth()->user();
        $orders = $me->orders()->with('lastStatus')->count();

        return response()->json($orders);
    }


    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    protected function index()
    {
        $me = auth()->user();
        $orderQuery = Order::where('customer_id', $me->id)->latest();
        $countOrder = $orderQuery->count();
        $orders = $orderQuery->with('service.user', 'lastStatus', 'statuses', 'material', 'customer')->get();
        if ($me->hasAnyRole('customer')) {
            // return response()->json($orders);
            return view('users/customer/order/customer-order', compact('orders', 'countOrder'));
        } elseif ($me->hasAnyRole('merchant')) {
            $orderQuery = $me->orders()->with('service.user', 'material', 'lastStatus', 'statuses')->latest();
            $orders = $orderQuery->get();
            $countOrder = $orderQuery->count();
            return view('users/merchant/order/merchant-order', compact('orders', 'countOrder'));
        } else {
            return redirect('/');
        }
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create($id)
    {   
        $me = auth()->user();
        if ($me->hasAnyRole('merchant')) {
            return redirect()->route('my_services');
        } elseif ($me->hasAnyRole('customer')) {
            if ($me->bank == null && $me->accountNumber == null) {
                return redirect()->route('account_edit')->withErrors('Lengkapi informasi account terlebih dahulu.');
            } else {
                $service = Service::where('id', $id)->with('user', 'materials')->first();
                return view('users/customer/order/create-order', compact('service', 'me'));
            }
        } else {
            return redirect('/');
        }
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $me = auth()->user();

        $validation = $request->validate([
            'order_note' => 'required|max:500',
            'material_id' => 'required|exists:materials,id',
            'copies' => 'required|integer|min:1|max:1000',
            'pick_up' => 'required',
            'file' => [
                new WithoutOther($request->fileLink, 'Berkas', 'Link berkas'),
                //'max: 100000', //200 Mb
            ],
        ]);

        $service = Service::find($request->service_id);
        $material = $service->materials()->where('id', $request->material_id)->first();
        $price = $material->price;
        $totalCost = $price * $request->copies;

        $orderCode = '';
        $character = array_merge(range('A','Z'), range('0','9'));
        $max = count($character) - 1;
        for ($i = 0; $i < 8; $i++) {
            $rand = mt_rand(0, $max);
            $orderCode .= $character[$rand];
        }
        $orderCode = $orderCode;

        if (request()->has('file')) {
            $file = $request->file('file');
            $filePath = $request->file('file')->storeAs('uploads/files', Str::random(40) . '.' . $file->getClientOriginalExtension(), 'public');
            $order = Order::create([
                'order_code' => $orderCode,
                'service_id' => $request->service_id,
                'material_id' => $material->id,
                'customer_id' => $me->id,
                'order_note' => $request->order_note,
                'copies' => $request->copies,
                'file' => $filePath,
                'total_cost' => $totalCost,
                'pick_up' => $request->pick_up,
            ]);
        } else {
            $order = Order::create([
                'order_code' => $orderCode,
                'service_id' => $request->service_id,
                'material_id' => $material->id,
                'customer_id' => $me->id,
                'order_note' => $request->order_note,
                'copies' => $request->copies,
                'file' => $request->fileLink,
                'total_cost' => $totalCost,
                'pick_up' => $request->pick_up,
            ]);
        };

        $status = Status::create([
            'order_id' => $order->id,
            'title' => 'Pending',
            'message' => 'Menunggu konfirmasi pihak percetakan',
        ]);
        return redirect()->route('order');
    }
                
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $me = auth()->user();
        $order = Order::where('id', $id)->with('service.user', 'material', 'lastStatus', 'statuses', 'customer')->first();
        $merchant = $order->service->user->id;
        $customer = User::where('id', $order->customer_id)->first();
        if ($me->hasAnyRole('merchant')) {
            if ($me->id == $merchant) {
                // return response()->json($order);
                return view('users/merchant/order/show-order', compact('order'));
            } else {
                return redirect()->route('merchant_order');
            }
        } elseif ($me->hasAnyRole('customer')) {
            if ($me->id == $customer->id) {
                // return response()->json($order);
                return view('users/customer/order/show-order', compact('order'));
            } else {
                return redirect()->route('customer_order');
            }
        } else {
            return redirect()->route('explore');
        }
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    // public function update(Request $request, $id)
    public function update(Request $request)
    {   
        $order = auth()->user()->orders()->find($request->order_id);
        if ($request->decision == 0) {
            $order->status = "Declined";
        } $order->status = "Printing for proofing";
        $order->update();
        return redirect()->route('merchant_order');
    }
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        //
    }
    
    public function customerLocation($id)
    {
        $me = auth()->user();
        $order = Order::where('order_code', $id)->with('service.user', 'customer')->first();
        $merchant = $order->service->user->id;
        // return response()->json($order);
        // $customer = User::where('id', $order->customer_id)->first();
        if ($me->hasAnyRole('merchant')) {
            if ($me->id == $merchant) {
                // return response()->json($order);
                return view('users/merchant/order/customer-location', compact('order'));
            } else {
                return redirect()->route('merchant_order');
            }
        } else {
                return redirect()->route('home');
        }
    }
}