<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function explore()
    {   
        $me = auth()->user();
        if ($me->hasAnyRole('customer')) {
            return view('users/customer/customer-home');
        } else {
            return redirect()->route('my_services');
        }
        return redirect('/');
    }

}
