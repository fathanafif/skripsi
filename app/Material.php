<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    protected static $settings = [];

    protected $fillable = [
        'service_id', 'material_name', 'material_size', 'gramatur', 'price'
    ];

    protected $appends = [
        'real_material_name', // ini itu getRealMaterialNameAttribute()
        'real_material_size', // ini itu getRealMaterialSizeAttribute()
    ];

    public function service()
    {
        return $this->belongsTo('App\Service', 'service_id');
    }

    public function getRealMaterialNameAttribute()
    {
        return $this->getSettingValue('matNames')[$this->material_name];
    }

    public function getRealMaterialSizeAttribute()
    {
        return $this->getSettingValue('matSizes')[$this->material_size];
    }

    public function getSettingValue($key)
    {
        if (isset(static::$settings[$key])) {
            $setting = static::$settings[$key];
        } else {
            $setting = static::$settings[$key] = Setting::where('setting_key', $key)->first();
        }

        return unserialize($setting->setting_value);
    }
}
