<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\ImplicitRule;

class WithoutOther implements ImplicitRule
{
    protected $otherValue;
    protected $firstLabel;
    protected $secondLabel;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($otherValue, $firstLabel, $secondLabel)
    {
        $this->otherValue = $otherValue;
        $this->firstLabel = $firstLabel;
        $this->secondLabel = $secondLabel;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!empty($value) && !empty($this->otherValue)) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "Hanya boleh mengisi salah satu, antara {$this->firstLabel} atau {$this->secondLabel}.";
    }
}
