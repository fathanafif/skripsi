@extends('layouts.customer-app')

@section('content')
<div class="ecommerce-widget">
    
    <!-- Service List -->
    <div class="row" id="indexService">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 px-0">
            <div class="card px-3">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="mt-3"><b>{{ $service->real_name }}</b></h3>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col col-md-4 pt-1">
                            @if ($service->image == null)
                            <img class="card-img img-fluid" src="{{ asset('assets/noThumbnail.png') }}" alt="Card image cap">
                            @else 
                            <img class="card-img img-fluid" src="{{ asset('storage/' . $service->image) }}" alt="Card image cap">
                            @endif
                        </div>
                        <div class="col col-md-8">
                            <div class="row">
                                <h5 class="col col-md-12 font-weight-bold"><a href="{{ route("show_merchant", $service->user->id) }}"><img class="user-avatar-md rounded-circle mr-2" src="{{ $service->user->profilePhoto == null ? asset('assets/blankHead.png') : asset('storage/' . $service->user->profilePhoto)}}" alt="">{{ $service->user->name }}</a></h5>
                            </div>
                            <div class="row pb-3">
                                <p class="col col-md-5">Estimasi waktu pengerjaan: <span id="secondary">{{ $service->estimated_time }} jam</span></p>
                            </div>
                            <div class="row">
                                <p class="col col-md-5">Deskripsi service:</p>
                            </div>
                            <div class="row pb-3">
                                <p class="col col-md-12 font-weight-light">{{ $service->description }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="row py-3">
                        <div class="col col-md-12">
                            <div class="row pt-2 pb-4">
                                <p class="px-3"><b>Bahan cetak yang tersedia:</b></p>
                            </div>
                            <div class="row pb-3 px-2 justify-content-center">
                                <div class="row col col-md-12 mb-2 px-4">
                                    @foreach ($service->materials as $material)
                                    <div class="card-body bg-white col col-md-3 px-2 pt-0 pb-4">
                                        <div class="card-text border pb-2">
                                            <p class="card-title p-2 mb-0">{{ $material->real_material_name }}</p>
                                            <small class="font-weight-bold">
                                                <p class="card-title px-2 mb-0">{{ $material->real_material_size . " " . $material->gramatur . " gsm" }} </p>
                                            </small>
                                            <p class="card-title px-2 mb-0 font-weight-bold" id="secondary">
                                                Rp {{ number_format($material->price,0,',','.') }}
                                                <small class="font-weight-bold"> / lembar</small>
                                            </p>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="row col float-right">
                                <div class="col col-md-2 offset-md-10">
                                    <a href="{{ route('create_order', $service->id) }}"><button class="btn btn-md btn-danger"><b>Order Sekarang</b></button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>      
        </div>    
    </div>
    @endsection
    
    @section('scriptPartialCustomer')
    @include('users._partial._customer-js')
    @endsection