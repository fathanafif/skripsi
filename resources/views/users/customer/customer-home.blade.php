@extends('layouts.customer-app')

@section('content')
<!-- Content Section -->
<div class="ecommerce-widget">
   
   <!-- Content -->
   <div class="row" ng-app="customerAngularJS">
      <div class="card col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 px-3" ng-controller="indexService">
         <div class="card-body mb">
            <div class="row py-5">
               <div class="search">
                  <div class="search_wrapper">
                     <input type="text" name="search" placeholder="Search" class="search_field py-4 px-3"  ng-model="result">
                  </div>
               </div>
            </div>
            <div class="row col col-md-12 mx-auto px-0 py-4" id="explore">
               <div class="card-body card-hover bg-white col col-md-3 px-2 mt-4 py-0" id="indexService" ng-repeat="ds in dataService | filter:result | orderBy:'id'">
                  <a href="service/@{{ ds.id }}">
                     <img class="card-img-top img-fluid" ng-if="ds.image != NULL" src="{{ asset('storage') }}/@{{ ds.image }}" alt="Card image cap">
                     <img class="card-img-top img-fluid" ng-if="ds.image == NULL" src="{{ asset('assets/noThumbnail.png') }}" alt="Card image cap">
                     <div class="card-text border pb-3">
                        <h5 class="card-title px-2 py-2 mb-0"><b>@{{ ds.real_name }}</b></h5>
                        <h6 class="font-weight-bold">
                           <i class="icon-store pl-2 pr-1" id="exploreCard"></i>@{{ ds.merchant }}
                        </h6>
                        <p class="px-2 mt-1 text-right">
                           @for ($i = 0; $i < 1; $i++)
                              <i class="icon-star" style="color: grey"></i>
                           @endfor
                           @for ($i = 0; $i < 4; $i++)
                              <i class="icon-star" style="color: orange"></i>
                           @endfor
                        </p>
                     </div>
                  </a>
               </div>
            </div>
         </div>
      </div>
   </div>
   
   @endsection
   
   @section('scriptPartialCustomer')
   @include('users._partial._customer-js')
   @endsection