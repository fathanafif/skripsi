@extends('layouts.customer-app')

@section('content')
<!-- Content Section -->
<div class="ecommerce-widget">
  <!-- Content -->
  <div class="row">
    <div class="card col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 px-3">
      <div class="card-header">
        <div class="row">
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <h3 class="mt-3"><b>Edit Account</b></h3>
          </div>
        </div>
      </div>    
      <div class="card-body mb-5" ng-controller="editAccount">
        @if($errors->any())
        <div class="alert alert-danger col col-md-12 mb-5">
          <b>Unsuccessful!</b><span class="errorMessage"> {{ $errors->first() }}</span>
        </div>
        @endif
        <form method="POST" action="{{ route('account_update') }}" enctype="multipart/form-data">
          @csrf
          {{ method_field('PUT') }}
          <div class="row">
            <div class="col col-md-4"></div>
            <div class="col col-md-8 px-0">
              <h4 class="font-weight-bold">Informasi Umum</h4>
            </div>
          </div>
          <div class="row">
            <div class="col col-md-4 pt-1 text-center pr-5">
              <img class="user-avatar-xxxl" src="{{ $me->profile_photo == NULL ? asset('assets/blankHead.png') : asset('storage/' . $me->profile_photo) }}" alt="Card image cap">
              <div class="row pt-5">
                <div class="col-md-10">
                  <input type="file" name="profile_photo" id="custom-file-input">
                </div>
                <div class="col-md-2 py-1">
                  <a id="remove_button">
                    <i class="icon-trash-2" id="cancelUpload"></i>
                  </a>
                </div>
              </div>
            </div>
            <div class="col col-md-8">
              <div class="row mb-3">
                <div class="col col-md-12">
                  <div class="row">
                    <label for="name">Nama Pelanggan</label>
                  </div>
                  <div class="row">
                    <input type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $me->name }}" placeholder="Tony Stark" required>
                    @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
                  </div>
                </div>
              </div>
              <div class="row mb-3">
                <div class="col col-md-12">
                  <div class="row">
                    <label for="email">Email</label>
                  </div>
                  <div class="row">
                    <input type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $me->email }}" placeholder="tonystark@gmail.com" required>
                    @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                  </div>
                </div>
              </div>
              <div class="row mb-3">
                <div class="col col-md-12">
                  <div class="row">
                    <label for="phone_number">Nomor telepon</label>
                  </div>
                  <div class="row">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <div class="input-group-text prepend">+62</div>
                      </div>
                      <input class="form-control {{ $errors->has('phone_number') ? 'is-invalid' : '' }}" id="phoneNumber" type="text" name="phone_number" value="{{ $me->phone_number }}" maxlength="12" placeholder="81234567890" required>
                      @if ($errors->has('phone_number'))
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('phone_number') }}</strong>
                      </span>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
              <div class="row pt-4">
                <h4 class="font-weight-bold">Rekening Pembayaran</h4>
              </div>
              <div class="row mb-3">
                <div class="col col-md-12">
                  <div class="row">
                    <label for="bank">Bank</label>
                  </div>
                  <div class="row">
                    <div class="input-group">
                      <select class="form-control" id="bank_names" name="bank" required>
                        <option value="">Bank name</option>
                        @foreach($setBank as $key => $valueItem)
                        <option value="{{ $key }}">{{ $valueItem }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row mb-3">
                <div class="col col-md-12">
                  <div class="row">
                    <label for="account_name">Nama pemilik rekening (atas nama)</label>
                  </div>
                  <div class="row">
                    <input id="account_name" type="text" class="form-control{{ $errors->has('account_name') ? ' is-invalid' : '' }}" name="account_name" value="{{ $me->account_name }}" required>
                    @if ($errors->has('account_name'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('account_name') }}</strong>
                    </span>
                    @endif
                  </div>
                </div>
              </div>
              <div class="row mb-3">
                <div class="col col-md-12">
                  <div class="row">
                    <label for="account_number">Nomor rekening</label>
                  </div>
                  <div class="row">
                    <div class="input-group">
                      <input id="account_number" type="text" class="form-control {{ $errors->has('account_number') ? 'is-invalid' : '' }}" name="account_number" value="{{ $me->account_number }}" required>
                      @if ($errors->has('account_number'))
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('account_number') }}</strong>
                      </span>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
              <div class="row pt-4">
                <h4 class="font-weight-bold">Lokasi Anda</h4>
              </div>
              <div class="row mb-3">
                <div class="col col-md-12 pl-0">
                  <div class="row pl-3">
                    <div class="gmaps" id="map"></div>
                  </div>
                  <div class="row">
                    <div class="input-group col-md-6 pl-3">
                      <input class="form-control col" id="lat" type="text" class="form-control {{ $errors->has('lat') ? 'is-invalid' : '' }}" name="lat" value="{{ $me->lat }}" required>
                      @if ($errors->has('lat'))
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('lat') }}</strong>
                      </span>
                      @endif
                    </div>
                    <div class="input-group col-md-6 pl-0">
                      <input class="form-control col" id="long" type="text" class="form-control {{ $errors->has('long') ? 'is-invalid' : '' }}" name="long" value="{{ $me->long }}" required>
                      @if ($errors->has('long'))
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('long') }}</strong>
                      </span>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-md-right mt-4 px-0">
                  <button type="submit" class="btn btn-purple text-right">
                    {{ __('Save') }}
                  </button>
                </div>
              </div>
            </div>
        </form>
      </div>
    </div>
  </div>
    
@endsection

@section('scriptPartialCustomer')
  @include('users._partial._customer_account-js')
@endsection