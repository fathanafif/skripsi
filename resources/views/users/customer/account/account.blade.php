@extends('layouts.customer-app')

@section('content')
<!-- Content Section -->
<div class="ecommerce-widget">
  
  <!-- Content -->
  <div class="row">
    <div class="card col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 px-3" ng-controller="customerAccount">
      <div class="card-header">
        <div class="row">
          <div class="col-xl-11 col-lg-11 col-md-11 col-sm-8 col-8">
            <h3 class="mt-3"><b>My Account</b></h3>
          </div>
          <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1 text-right my-auto">
            <a href="{{ route('account_edit') }}" data-toggle="tooltip" data-placement="left" title="Edit Account Info"><i class="icon-edit-3" id="editMerchantAccount"></i></a>
          </div>
        </div>
      </div>
      <div class="card-body mb-5">
        <div class="row">
          <h4 class="col col-md-7 offset-md-4 font-weight-bold pb-4"><b>Informasi Customer</b></h4>
        </div>
        <div class="row">
          <div class="col col-md-4 text-center">
            <img class="user-avatar-xxxl" src="{{ $me->profilePhoto == NULL ? asset('assets/blankHead.png') : asset('storage/' . $me->profilePhoto) }}" alt="Card image cap">
          </div>
          <div class="col col-md-8">
            <div class="row">
              <p class="col col-md-5 font-weight-bold">Nama<span class="float-right">:</span></p>
              <p class="col col-md-7 font-weight-bold">{{ $me->name }}</p>
            </div>
            <div class="row">
              <p class="col col-md-5 font-weight-bold">Email<span class="float-right">:</span></p>
              <p class="col col-md-7 font-weight-bold">{{ $me->email }}</p>
            </div>
            <div class="row">
              <p class="col col-md-5 font-weight-bold">Nomor telepon<span class="float-right">:</span></p>
              <p class="col col-md-7 font-weight-bold">{{ $me->phone_number == NULL ? "..." : "+62" . $me->phone_number }}</p>
            </div>
            <div class="row pt-3">
              <h4 class="col col-md-7 font-weight-bold"><b>Rekening Pembayaran</b></h4>
            </div>
            <div class="row">
              <p class="col col-md-5 font-weight-bold">Bank<span class="float-right">:</span></p>
              <p class="col col-md-7 font-weight-bold">{{ $me->bank == null ? '...' : $setBank[$me->bank] }}</p>
            </div>
            <div class="row">
              <p class="col col-md-5 font-weight-bold">Atas nama<span class="float-right">:</span></p>
              <p class="col col-md-7 font-weight-bold">{{ $me->account_name == null ? '...' : $me->account_name}}</p>
            </div>
            <div class="row">
              <p class="col col-md-5 font-weight-bold">Nomor rekening<span class="float-right">:</span></p>
              <p class="col col-md-7 font-weight-bold">{{ $me->account_number == null ? '...' : $me->account_number }}</p>
            </div>
          </div>
        </div>
        <div class="row pt-3">
          <h4 class="col col-md-12 font-weight-bold"><b>Lokasi Percetakan</b></h4>
        </div>
        <div class="row">
          @if ($me->lat == null && $me->long == null)
          <p class="col">Anda belum memasukkan data lokasi.</p>
          @else
          <div class="col col-md-12">
              <div class="gmaps" id="map"></div>
          </div>
          @endif
        </div>
        <div class="row float-right pb-3">
          <small class="font-weight-bold" id="lat">{{ $me->lat }}</small>
          <small class="pr-1 font-weight-bold" id="lat">,</small>
          <small class="pr-3 font-weight-bold" id="lng">{{ $me->long }}</small>
        </div>
      </div>
    </div>
  </div>
  
  @endsection
  
  @section('scriptPartialCustomer')
    @include('users._partial._customer_account-js')
  @endsection

 