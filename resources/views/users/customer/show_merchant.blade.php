@extends('layouts.customer-app')

@section('content')
<div class="ecommerce-widget">
    <!-- Service List -->
    <div class="row" id="indexService">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 px-0">
            <div class="card px-3">
                <div class="card-body" ng-controller="merchantInfo">
                    <div class="row">
                        <div class="col col-md-2 text-center">
                            <img class="user-avatar-xl rounded-circle" src="{{ $merchant->profilePhoto == null ? asset('assets/blankHead.png') : asset('storage/' . $merchant->profilePhoto)}}" alt="">
                        </div>
                        <div class="col col-md-10">
                            <div class="row">
                                <div class="col col-md-12"><h3 class="font-weight-bold">{{ $merchant->name }}</h3></div>
                            </div>
                            <div class="row py-1">
                                <div class="col col-md-2">Email</div>
                                <div class="col col-md-4">: {{ $merchant->email }}</div>
                                <div class="col col-md-2">Bergabung sejak</div>
                                <div class="col col-md-4">: {{ $merchant->created_at->format('M Y') }}</div>
                            </div>
                            <div class="row py-1">
                                <div class="col col-md-2">Nomor telepon</div>
                                <div class="col col-md-4">: +62{{ $merchant->phone_number }}</div>
                                <div class="col col-md-2">Service yang dimiliki</div>
                                <div class="col col-md-4">: {{ $merchant->services->count() }}</div>
                                
                            </div>
                            <div class="row py-1">
                                <div class="col col-md-2">Pembayaran</div>
                                <div class="col col-md-4">: {{ $merchant->real_bank_name }}</div>
                                <div class="col col-md-2">Order sukses</div>
                                <div class="col col-md-4">: -</div>
                            </div>
                            <div class="row py-1" hidden>
                                <input type="text" name="" id="lat" value="{{ $merchant->lat }}">
                                <input type="text" name="" id="lng" value="{{ $merchant->long }}">
                            </div>
                        </div>
                    </div>
                    <div class="row pt-5">
                        <div class="col">
                            <div>
                                @foreach ($merchant->services as $service)
                                    <div class="col col-md-3">    
                                        <div class="card card_hover">
                                            <a class="stretched-link" href="{{ route('show_service', $service->id) }}">
                                                <div class="card-body">
                                                    blablabla
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>      
    </div>
@endsection
    
@section('scriptPartialCustomer')
@include('users._partial._customer_merchant_info-js')
@endsection