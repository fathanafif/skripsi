@extends('layouts.customer-app')

@section('content')
<!-- Content Section -->
<div class="ecommerce-widget">
    
    <!-- Content -->
    <div class="row">
        <div class="card col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 px-3">
            <div class="card-header">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <h3 class="mt-3"><b>My Orders</b></h3>
                    </div>
                </div>
            </div>
            <div class="card-body mb-5">
                <div class="row">
                    <div class="card col col-md-12 py-3">
                        <div class="col-md-2">Jumlah Order: <b class="heading text-primary">{{ $countOrder }}</b></div>
                    </div>
                </div>
                <div class="row">
                    @foreach ($orders as $order)
                    <div class="card col-md-12 card-hover px-0">
                        <a href="{{ route('show_order', $order->id) }}" class="stretched-link">
                            <div class="card-body p-0">
                                <div class="row py-3 px-3">
                                    <div class="col-md-3 px-3 card-img-top my-auto">
                                        <img class="card-img img-fluid text-center" src="{{ $order->service->image == null ? asset('assets/noThumbnail.png') : asset('storage/' . $order->service->image) }}" alt="">
                                        @if (substr($order->file, -3) == "psd" || substr($order->file, -3) == ".ai" || substr($order->file, -3) == "cdr" || substr($order->file, -3) == "pdf" )
                                            <div class="pt-1 pb-3">
                                                <img src="{{ $order->thumbnail }}" width="20">
                                            </div>
                                        @else
                                            <div class="pt-1 pb-3">
                                                <i class="icon-link pl-2"></i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col col-md-3 py-2">
                                        <div>Order Code:</div>
                                        <div class="order-code"><h4 class="mb-3"><b>{{ $order->order_code }}</b></h4></div>
                                        <div><p class="mb-4"><i class="icon-store"></i> {{ $order->service->user->name }}</p></div>
                                        <div>Tanggal Order:</div>
                                        <div class="order-date"><p><i class="icon-clock1 pr-2"></i> {{ $order->created_at->format('H:i') }} WIB</p></div>
                                        <div class="order-date"><p><i class="icon-calendar1 pr-2"></i> {{ $order->created_at->format('d M Y') }}</p></div>
                                    </div>
                                    <div class="col col-md-3 py-2">
                                        <div>Jenis Cetak:</div>
                                        <div><h4><b>{{ $order->material->service->real_name }}</b></h4></div>
                                        <div>Bahan:</div>
                                        <div><h4 class="mb-0"><b>{{ $order->material->real_material_name }}</b></h4></div>
                                        <div>{{ $order->material->real_material_size . " " . $order->material->gramatur . " gsm" }}</div>
                                        <div>Rp {{ number_format($order->material->price,0,',','.') }}/lembar</div>
                                        <div>{{ $order->copies }} rangkap</div>
                                    </div>
                                    <div class="col col-md-3 py-2">
                                        <div>Total Biaya:</div>
                                        <div><h4><b name="totalBiaya">Rp {{ number_format($order->total_cost,0,',','.') }}</b></h4></div>
                                        <div>Status:</div>
                                        <div class="order-status">
                                            @include('users._partial.customer_status_order')
                                        </div>
                                        <div class="pt-3">Pengambilan Barang:</div>
                                        <div>
                                            {{ $order->pick_up == 0 ? "Kirim ke lokasi saya" : "Ambil ke percetakan" }}
                                        </div>
                                    </div>                           
                                </div>
                            </div>
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    
    @endsection
    
    @section('scriptPartialCustomer')
    @include('users._partial._customer-js')
    @endsection