@extends('layouts.customer-app')

@section('content')
<div class="ecommerce-widget">
    
    <!-- Content -->
    <div class="row">
        <div class="card col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 px-3 pb-5">

            {{-- SPECIFICATION --}}
            <div class="row pb-2 pt-5 px-3">
                <div class="col col-md-3 px-3 card-img-top">
                        <img class="card-img img-fluid text-center" src="{{ $order->service->image == null ? asset('assets/noThumbnail.png') : asset('storage/' . $order->service->image) }}" alt="">
                        @if (substr($order->file, -3) == "psd" || substr($order->file, -3) == ".ai" || substr($order->file, -3) == "cdr" || substr($order->file, -3) == "pdf" )
                            <div class="py-2">
                                <div class="row col-md-10 offset-md-1 justify-content-around">
                                    <img class="" src="{{ $order->thumbnail }}" width="28">
                                    <a href="{{ asset('storage') . "/" . $order->file }}" download="{{ $order->file }}"><button type="button" class="btn btn-xs btn-purple px-2">Download file<i class="icon-download1 pl-2"></i> </button></a>
                                </div>
                            </div>
                        @else
                            <div class="py-2">
                                <div class="row col-md-8 offset-md-2 justify-content-around">
                                    <a href="{{ $order->file }}" target="_blank"><i class="icon-link pt-1 pr-2"></i>Visit file link</a>
                                </div>
                            </div>
                        @endif
                    </div>
                <div class="col col-md-3 px-auto">
                    <div>Order Code:</div>
                    <div class="order-code"><h4 class="mb-3"><b>{{ $order->order_code }}</b></h4></div>
                    <div><p class="mb-4">
                        <a href="{{ route('show_merchant', $order->service->user->id) }}">
                            <i class="icon-store pr-2"></i> {{ $order->service->user->name }}</p>
                        </a>
                    </div>
                    <div class="order-date">Tanggal Order:</div>
                    <div><p><i class="icon-clock1 pr-2"></i> {{ $order->created_at->format('H:i') }} WIB</p></div>
                    <div><p><i class="icon-calendar1 pr-2"></i> {{ $order->created_at->format('d M Y') }}</p></div>
                </div>
                <div class="col col-md-3 px-auto">
                    <div>Jenis Cetak:</div>
                    <div><h4><b>{{ $order->material->service->real_name }}</b></h4></div>
                    <div>Bahan:</div>
                    <div><h4 class="mb-0"><b id="materialName">{{ $order->material->real_material_name }}</b></h4></div>
                    <div id="materialSize">{{ $order->material->real_material_size . " " . $order->material->gramatur . " gsm" }}</div>
                    <div id="hargaSatuan">Rp {{ number_format($order->material->price,0,',','.') }}/lembar</div>
                    <div id="copies">{{ $order->copies }} rangkap</div>
                </div>
                <div class="col col-md-3 px-auto">
                    <div>Total Biaya:</div>
                    <div><h4><b>Rp {{ number_format($order->total_cost,0,',','.') }}</b></h4></div>
                    <div>Pengambilan Barang:</div>
                    <div>
                        @if ($order->pick_up == 0)
                            Kirim ke lokasi saya
                            <small class="row font-weight-bold pl-3">{{ $order->pick_up == 0 ? "Biaya pengiriman dibebankan kepada customer." : "" }}</small>
                        @else
                        Ambil ke percetakan<a href={{ route('account') }}><i class="icon-map-pin pl-2 pt-1" data-toggle="tooltip" data-placement="top" title="Tooltip on top"></i></a><br>
                        @endif
                    </div>
                    <div class="mt-4">Status Order:</div>
                    <div class="mb-4 order-status">
                        @include('users._partial.customer_status_order')
                    </div>
                </div>
            </div>
            
            {{-- ORDER NOTE --}}
            <div class="row offset-md-3">
                <div class="col-md-2">Catatan Order:</div>
            </div>
            <div class="row offset-md-3 pb-4">
                <div class="col font-weight-light order-note">{{ $order->order_note }}</div>
            </div>

            {{-- ORDER TRACK --}}
            @if ($order->copies > 1)
                @include('users._partial.customer_track_order')
            @else
                @include('users._partial.customer_track_order_without_proofing')
            @endif

            {{-- ACTION --}}
            @if ($order->copies > 1)
                <div class="row mx-3 py-3 border-top">
                    @if ($order->lastStatus->proofing != null)
                        <div class="col-md-9 py-2">Apakah hasil proofing sudah sesuai dengan harapan Anda? Tekan "Process" jika sesuai atau "Cancel order" jika tidak.</div>
                        <div class="col-md-3 text-right">
                            <form action="{{ route('store_status') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <input type="text" name="order_id" value="{{ $order->id }}" hidden>
                                <input type="text" name="proofingConfirmation" value="1" hidden>
                                <input class="btn btn-sm btn-link mr-2 font-weight-bold" name="decision" type="submit" value="Cancel order"/>
                                <button class="btn btn-sm btn-success font-weight-bold" name="proofingDecision" type="submit">Process</button>
                            </form>
                        </div>
                    @elseif ($order->lastStatus->title == "Confirmed by customer")
                        <div class="col-md-8">
                            <div class="row pt-3 pb-2">
                                <div class="col-md-4">Merchant</div>
                                <div class="col-md-1">:</div>
                                <div class="col-md-6"><img class="user-avatar-md rounded-circle mr-4" src="{{ $order->service->user->profilePhoto == null ? asset('assets/blankHead.png') : asset('storage/' . $order->service->user->profilePhoto)}}" alt="">{{ $order->service->user->name }}</div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-4">Rekening tujuan pembayaran</div>
                                <div class="col-md-1">:</div>
                                <div class="col-md-1">
                                    @if ($order->service->user->bank == "mandiri")
                                    <img src="{{ asset('assets/mandiri.svg') }}" alt="" width="32px">
                                    @elseif ($order->service->user->bank == "bni")
                                    <img id="bni" src="{{ asset('assets/bni.svg') }}" alt="" width="43px">
                                    @elseif ($order->service->user->bank == "bri")
                                    <img src="{{ asset('assets/bri.svg') }}" alt="" width="28px">
                                    @else
                                    <img src="{{ asset('assets/bca.svg') }}" alt="" width="28px">   
                                    @endif
                                </div>
                                <div class="col-md-6">{{ $order->service->user->account_number }} a/n {{ $order->service->user->account_name }}</div>
                            </div>

                            <div class="row pt-3 pb-5">
                                <div class="col-md-4">Total biaya yang dibayarkan</div>
                                <div class="col-md-1">:</div>
                                <div class="col-md-6 ofset-md-1">Rp {{ number_format($order->total_cost,0,',','.') }}</div>
                            </div>
                            
                            <div class="row">
                                <div class="col">
                                    Silahkan melakukan pembayaran dengan cara transfer, dapat melalui mesin ATM / mobile banking dengan rekening tujuan dan nominal sesuai yang tertera di atas serta <b class="text-danger">simpan bukti transfer</b>.
                                </div>
                            </div>
                        </div>
                        <div class="col col-md-4 border-left align-middle pt-5 px-auto">
                            <form action="{{ route('store_status') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="row col pt-4 mx-0">
                                    <input type="text" name="order_id" value="{{ $order->id }}" hidden>
                                    <label for="payment_slip">Silahkan kirim bukti pembayaran</label>
                                    <input class="form-group" type="file" name="payment_slip" id="custom-file-input" required>
                                </div>
                                <div class="row py-3 float-right pr-4">
                                    <button class="btn btn-sm btn-success font-weight-bold" name="sendPaymentSlip" type="submit">Send Payment Slip</button>
                                </div>
                            </form>
                        </div>
                    @elseif ($order->lastStatus->title == "Slip is Verified")
                        <div class="col col-md-12 pt-3 px-auto">
                            <div class="row py-2 justify-content-around">Menunggu order selesai dikerjakan oleh pihak percetakan (merchant).</div>
                        </div>
                    @else
                    <div class="col-md-12"></div>
                    @endif
                </div>

            @else
                <div class="row mx-3 py-3 border-top">
                @if ($order->lastStatus->title == "Confirmed by merchant")
                    <div class="col-md-8">
                        <div class="row pt-3 pb-2">
                            <div class="col-md-4">Merchant</div>
                            <div class="col-md-1">:</div>
                            <div class="col-md-6"><img class="user-avatar-md rounded-circle mr-4" src="{{ $order->service->user->profilePhoto == null ? asset('assets/blankHead.png') : asset('storage/' . $order->service->user->profilePhoto)}}" alt="">{{ $order->service->user->name }}</div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-4">Rekening tujuan pembayaran</div>
                            <div class="col-md-1">:</div>
                            <div class="col-md-1">
                                @if ($order->service->user->bank == "mandiri")
                                <img src="{{ asset('assets/mandiri.svg') }}" alt="" width="32px">
                                @elseif ($order->service->user->bank == "bni")
                                <img id="bni" src="{{ asset('assets/bni.svg') }}" alt="" width="43px">
                                @elseif ($order->service->user->bank == "bri")
                                <img src="{{ asset('assets/bri.svg') }}" alt="" width="28px">
                                @else
                                <img src="{{ asset('assets/bca.svg') }}" alt="" width="28px">   
                                @endif
                            </div>
                            <div class="col-md-6">{{ $order->service->user->account_number }} a/n {{ $order->service->user->account_name }}</div>
                        </div>

                        <div class="row pt-3 pb-5">
                            <div class="col-md-4">Total biaya yang dibayarkan</div>
                            <div class="col-md-1">:</div>
                            <div class="col-md-6 ofset-md-1">Rp {{ number_format($order->total_cost,0,',','.') }}</div>
                        </div>
                        
                        <div class="row">
                            <div class="col">
                                Silahkan melakukan pembayaran dengan cara transfer, dapat melalui mesin ATM / mobile banking dengan rekening tujuan dan nominal sesuai yang tertera di atas serta <b class="text-danger">simpan bukti transfer</b>.
                            </div>
                        </div>
                    </div>
                    <div class="col col-md-4 border-left align-middle pt-5 px-auto">
                        <form action="{{ route('store_status') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="row col pt-4 mx-0">
                                <input type="text" name="order_id" value="{{ $order->id }}" hidden>
                                <label for="payment_slip">Silahkan kirim bukti pembayaran</label>
                                <input class="form-group" type="file" name="payment_slip" id="custom-file-input" required>
                            </div>
                            <div class="row py-3 float-right pr-4">
                                <button class="btn btn-sm btn-success font-weight-bold" name="sendPaymentSlip" type="submit">Send Payment Slip</button>
                            </div>
                        </form>
                    </div>
                @elseif ($order->lastStatus->title == "Transfered")
                    <div class="row col-md-12 pt-2 justify-content-around">
                        "Menunggu merchant melakukan verifikasi pembayaran yang telah Anda lakukan"
                    </div>
                @elseif ($order->lastStatus->title == "Slip is verified")
                    <div class="row justify-content-around">"Order Anda sedang dalam proses pengerjaan".</div>
                @else
                    
                @endif

            @endif
            
        </div>
    </div>

@endsection

@section('scriptPartialCustomer')
@include('users._partial._customer-js')
@endsection