@extends('layouts.customer-app')

@section('content')
<!-- Content Section -->
<div class="ecommerce-widget">
  
  <!-- Content -->
  <div class="row">
    <div class="card col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 px-3">
      <div class="card-header">
        <div class="row">
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <h3 class="mt-3"><b>Order Checkout</b></h3>
          </div>
        </div>
      </div>
      <div class="card-body px-0" ng-controller="createOrder">
        <form action="{{ route('store_order') }}" method="POST" enctype="multipart/form-data">
          @csrf
          @if($errors->any())
          <div class="alert alert-danger">
            <strong>Gagal melakukan order!</strong> {{ $errors->first() }}
          </div>
          @endif
          <div class="row mb-4 px-4">
            <div class="col col-md-5">
              <img class="img-fluid" src="{{ $service->image == NULL ? asset('assets/noThumbnail.png') : asset('storage/' . $service->image) }}" alt="Card image cap">
            </div>
            <div class="col col-md-7">
              <div class="row px-3">
                <img class="user-avatar-md rounded-circle mr-2" src="{{ $service->user->profilePhoto == null ? asset('assets/blankHead.png') : asset('storage/' . $service->user->profilePhoto)}}" alt="">
                  <label class="col-md-6" style="padding-top: 5px;">
                    {{ $service->user->name }}
                    <a href=""><i class=" icon-external-link pl-2"></i></a>
                  </label>
              </div>
              <div class="row px-3 pt-3 pb-0">
                  <div class="col col-md-7 px-0">
                    <p>Estimasi waktu pengerjaan<span id="secondary"> {{ $service->estimated_time }} jam</span></p>
                  </div>
              </div>
              <div class="row px-3 pt-1 pb-0">
                  <div class="col col-md-7 px-0">
                    <p class="">Deskripsi service</p>
                  </div>
                  <input type="hidden" name="service_id" value="{{ $service->id }}">
              </div>
              <div class="row px-3 py-0">
                <div class="col col-md-12 px-0">
                  <p class="font-weight-light">{{ $service->description }}</p>
                </div>
              </div>
            </div>
          </div>
          <div class="row border-top pt-4 mx-1">
            <div class="col col-md-5 text-right">
              <label for="orderNote">Jenis layanan cetak</label>
            </div>
            <div class="col col-md-7">
              <label>{{ $service->real_name }}</label>
            </div>
          </div>
          <div class="row pt-4 mx-1">
            <div class="col col-md-5 text-right">
              <label class="my-auto align-middle" for="orderNote">Catatan order</label>
            </div>
            <div class="col col-md-7">
              <textarea class="form-control" id="orderNote" rows="4" name="order_note" placeholder="Catatan untuk merchant" required></textarea>
            </div>
          </div>
          <div class="row px-4 pt-3">
            <div class="col col-md-5 text-right">
              <label class="my-auto align-middle" for="material">Bahan & ukuran</label>
            </div>
            <div class="col col-md-7">
              <select class="form-control" name="material_id" id="material" required>
                <option value="">Pilih bahan</option>
                @foreach($service->materials as $key => $material)
                <option value="{{ $material->id }}">{{ $material->real_material_name . " " . $material->gramatur . "gsm " . $material->real_material_size . " Rp " .  $material->price . "/lembar" }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="row px-4 pt-3 pb-3">
            <div class="col col-md-5 text-right">
              <label class="my-auto align-middle" for="copies">Jumlah rangkap</label>
            </div>
            <div class="col col-md-2">
              <input class="form-control" id="copies" name="copies" type="number" min="1" max="1000" value="1" required>
            </div>
          </div>
          <div class="row px-4">
            <div class="col col-md-5 text-right">
              <label class="my-auto align-middle" for="file">Berkas</label>
            </div>
            <div class="col col-md-6 pb-1 pr-0">
              <input class="form-group" type="file" name="file" id="custom-file-input"><br>
              <p class="font-weight-light">Atau menggunakan link Google Drive.</p>
            </div>
            <div class="col-md-1 py-1 pl-4">
              <a id="remove_button">
                <i class="icon-trash-2" id="cancelUpload"></i>
              </a>
            </div>
          </div>
          <div class="row px-4 pb-5">
            <div class="col col-md-5 text-right">
              <label class="my-auto align-middle" for="fileLink">Link berkas</label>
            </div>
            <div class="col col-md-7">
              <input class="form-control" type="url" name="fileLink" placeholder="https://drive.google.com/open?id=1A2b3C4d5E6f7G">
              {{-- <input class="form-control" type="url" id="custom-file-input" value="{{ $me->name }}" readonly> --}}
            </div>
          </div>
          <div class="row px-4 pb-2">
            <div class="col col-md-5 text-right">
              <label class="my-auto align-middle" for="file">Nama</label>
            </div>
            <div class="col col-md-7 pb-1">
                <input class="form-control" type="text" id="custom-file-input" value="{{ $me->name }}" readonly>
            </div>
          </div>
          <div class="row px-4 pb-2">
            <div class="col col-md-5 text-right">
              <label class="my-auto align-middle" for="file">Email</label>
            </div>
            <div class="col col-md-7 pb-1">
                <input class="form-control" type="text" id="custom-file-input" value="{{ $me->email }}" readonly>
            </div>
          </div>
          <div class="row px-4 pb-2">
            <div class="col col-md-5 text-right">
              <label class="my-auto align-middle" for="file">Nomor telepon</label>
            </div>
            <div class="col col-md-7 pb-1">
                <input class="form-control" type="text" id="custom-file-input" value="+62{{ $me->phone_number }}" readonly>
            </div>
          </div>
          <div class="row px-4 pb-2">
            <div class="col col-md-5 text-right">
              <label class="my-auto align-middle" for="file">Bank</label>
            </div>
            <div class="col col-md-7 pb-1">
                <input class="form-control" type="text" id="custom-file-input" value="{{ $me->bank }}" readonly>
            </div>
          </div>
          <div class="row px-4 pb-2">
            <div class="col col-md-5 text-right">
              <label class="my-auto align-middle" for="file">Rekening Pembayaran</label>
            </div>
            <div class="col col-md-7 pb-1">
                <input class="form-control" type="text" id="custom-file-input" value="{{ $me->account_number }} a/n {{ $me->account_name }}" readonly>
            </div>
          </div>
          <div class="row px-4 pt-5">
            <div class="col col-md-5 text-right">
              <label class="my-auto align-middle" for="pick_up">Pengambilan barang</label>
            </div>
            <div class="col col-md-3 pb-2">
              <select class="form-control" name="pick_up" id="pick_up" required>
                <option value="">Pilih pengambilan barang</option>
                <option value="0">Kirim ke lokasi saya</option>
                <option value="1">Ambil ke lokasi percetakan</option>
              </select>
            </div>
          </div>
          <div class="row pb-2 offset-md-5">
            <div class="font-weight-light pl-3">
              <a href="{{ route('account_edit') }}" class="pr-4 mr-3">Ubah lokasi pengiriman</a>
              <a href="{{ route('show_merchant', $service->user->id) }}" class="">Lihat lokasi percetakan</a>
            </div>
          </div>
          <div class="card-body border-top py-0">
            <div class="form-group row pl-3">
              <div class="col col-md-10 custom-control custom-checkbox">
                <input type="checkbox" name="checkBox" class="custom-control-input" id="customCheck1">
                <label class="custom-control-label font-weight-light" for="customCheck1">Data yang diinputkan sudah sesuai dan file yang saya unggah sudah menggunakan kode warna CMYK<br>(sehingga warna hasil cetak sesuai dengan warna pada file).</label>
              </div>
              <div class="col col-md-2 text-right pt-2">
                <button class="btn btn-danger font-weight-bold" type="submit" name="button">Order</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
    
    @endsection
    
    @section('scriptPartialCustomer')
    @include('users._partial._customer-js')
    @endsection