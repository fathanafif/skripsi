@extends('layouts.merchant-app')

@section('content')
<!-- Content Section -->
<div class="ecommerce-widget">
  
  <!-- Service List -->
  <div class="row" id="indexService">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 px-0">
      <div class="card px-3">
        <div class="card-header">
          <div class="row">
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <h3 class="mt-3"><b>My Services</b></h3>
              </div>
            </div>
        </div>
        <div class="card-body">
          <div class="row px-2 mb-5">
            @foreach ($services as $service)
            <div class="card-body card-hover bg-white col col-md-3 px-2 pt-0 mt-4 pb-0">
              <a href="{{ route('show_service', $service->id) }}">
                <img class="card-img-top img-fluid" src="{{ $service->image == NULL ? asset('assets/noThumbnail.png') : asset('storage/' . $service->image) }}" alt="Card image cap">
                <div class="card-text border pb-2">
                  <h5 class="card-title px-2 pt-2 mb-0" id="card-title"><b>{{ $setService[$service->name] }}</b></h5>
                  <p class="card-title px-2 mb-0">{{ $service->materials->count() }} Material</p>
                  <p class="card-title px-2 mb-0 text-right">
                    @for ($i = 0; $i < 1; $i++)
                      <i class="icon-star" style="color: grey"></i>
                    @endfor
                    @for ($i = 0; $i < 4; $i++)
                      <i class="icon-star" style="color: orange"></i>
                    @endfor
                  </p>
                </div>
              </a>
            </div>
            @endforeach
          </div>
        </div>
      </div>      
    </div>    
  </div>
  
  @endsection
  
  @section('scriptPartialMerchant')
  @include('users._partial._merchant-js')
  @endsection