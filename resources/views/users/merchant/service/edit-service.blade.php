@extends('layouts.merchant-app')

@section('content')
<!-- Content Section -->
<div class="ecommerce-widget">
  
  <!-- Content -->
  <div class="row">
    <div class="card col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 px-3">
      <div class="card-header">
        <div class="row">
          <div class="col-xl-11 col-lg-11 col-md-11 col-sm-8 col-8">
            <h3 class="mt-3"><b>Edit Service</b></h3>
          </div>
        </div>
      </div>
      <div class="card-body mb-5">
        <form action="{{ route('store_service') }}" method="POST" enctype="multipart/form-data">
          @csrf
          @if($errors->any())
          <div class="alert alert-danger">
            <strong>Gagal membuat service!</strong> {{ $errors->first() }}
          </div>
          @endif
          
          <div class="row">
            <div class="col-md-12">
              <h4><b>Deskripsi umum layanan cetak</b></h4>
            </div>
          </div>
          <div class="form-group row pb-1">
            <div class="col col-md-3">
              <label for="serviceName" class="col-form-label">Jenis cetak</label>
            </div>
            <div class="col col-md-3">
              <div class="input-group">
                <select class="form-control" name="serviceName" required>
                  <option value="">Jenis layanan cetak</option>
                  @foreach($itemsService as $key => $valueItem)
                  <option value="{{ $key }}">{{ $valueItem }}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>
          <div class="row offset-md-3 pl-2 pt-0"><small>* Wajib diisi</small></div>
          <div class="form-group row pb-1">
            <div class="col col-md-3">
              <label for="estimatedTime">Estimasi waktu pengerjaan</label>
            </div>
            <div class="col col-md-2">
              <input class="form-control" id="estimatedTime" name="estimatedTime"/>
            </div>
          </div>
          <div class="row offset-md-3 pl-2 pt-0"><small>* Wajib diisi (dalam ukuran jam)</small></div>
          <div class="form-group row pb-1">
            <div class="col col-md-3">
              <label for="description">Deskripsi</label>
            </div>
            <div class="col col-md-9">
              <textarea class="form-control" id="description" rows="4" name="description" required placeholder="Deskripsi layanan cetak (max: 200 karakter)"></textarea>
            </div>
          </div>
          <div class="row offset-md-3 pl-2 pt-0"><small>* Wajib diisi</small></div>
          <div class="row mt-5">
            <div class="col col-md-12">
              <h4><b>Jenis bahan yang tersedia</b></h4>
            </div>
          </div>
          <div class="form-group row">
            <div class="col col-md-3">
              <label for="materialName[]" class="col-form-label">Bahan yang tersedia (max:10)</label>
            </div>
            <div class="col col-md-9">
              <div class="input_fields_wrap" id="field_wrapper">
                <div class="form-group row py-0">
                  <div class="col col-md-3 input-group">
                    <select class="form-control" name="materialName[]" id="materialName" required>
                      <option value="">Nama bahan</option>
                      @foreach($materialNames as $key => $valueItem)
                      <option value="{{ $key }}">{{ $valueItem }}</option>
                      @endforeach
                    </select>                  
                  </div>  
                  <div class="col col-md-2 input-group">
                    <input type="text" class="form-control" id="gramatur" name="gramatur[]" placeholder="Gramatur">                  
                  </div>  
                  <div class="col col-md-3 input-group">
                    <select class="form-control" name="materialSize[]" id="materialSize" required>
                      <option value="">Ukuran bahan</option>
                      @foreach($materialSizes as $key => $valueItem)
                      <option value="{{ $key }}">{{ $valueItem }}</option>
                      @endforeach
                    </select>                  
                  </div>  
                  <div class="col col-md-3 input-group">
                    <div class="input-group-prepend"><span class="input-group-text">Rp</span></div>
                    <input type="text" class="form-control" id="price" placeholder="Harga" name="price[]" required min="4" max="5">
                  </div>
                </div>
                <div class="form-group row pt-0">
                  <div class="col col-md-3">
                    <small>* Wajib diisi</small>
                  </div>
                  <div class="col col-md-2">
                    <small>* Hanya kertas</small>
                  </div>
                  <div class="col col-md-3">
                    <small>* Wajib diisi</small>
                  </div>
                  <div class="col col-md-3">
                    <small>* Wajib diisi</small>
                  </div>
                </div>
              </div>
              <div class="input_fields_wrap mt-4">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-right mt-4 px-0">
                  <button type="button" name="button" class="btn btn-xs btn-secondary text-right" id="add_button"><i class="icon-plus1 pr-2"></i>Material</button>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col col-md-12">
              <h4><b>Gambar sempel cetak</b></h4>
            </div>            
          </div>
          <div class="form-group row pb-1">
            <div class="col col-md-3">
              <label for="photoUpload">Unggah gambar</label>
            </div>
            <div class="col col-md-8">
              <input class="" type="file" name="image" id="custom-file-input">
            </div>
          </div>
          <div class="row offset-md-3 pl-2 pb-5"><small>* Opsional</small></div>
        </div>
        <div class="card-body border-top">
          <div class="form-group row">
            <div class="col col-md-12">
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-md-right mt-2 px-0">
                <button type="submit" class="btn btn-purple text-right" name="button">Save</button>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
  
  @endsection
  
  @section('scriptPartialMerchant')
  @include('users._partial._merchant-js')
  @endsection