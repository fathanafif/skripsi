@extends('layouts.merchant-app')

@section('content')
<!-- Content Section -->
<div class="ecommerce-widget">
  <!-- Title & Breadcrumb -->

  <!-- Content -->
  <div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-xl-10 col-lg-10 col-md-10 col-sm-10 col-10">
                <h3 class="mt-3">Change My Password</h3>
            </div>
          </div>
        </div>
        <div class="card-body pl-5">
          <form method="POST" action="{{ route('merchant_updatePassword', $me->id) }}">
            @csrf
            {{ method_field('PUT') }}
            <div class="form-group row">
              <div class="col col-md-3">
                <label class="pt-1" for="current-password">Current Password</label>
              </div>
              <div class="col col-md-4">
                <input type="password" class="form-control" name="current-password" required>
              </div>
            </div>
            <div class="form-group row">
              <div class="col col-md-3">
                <label class="pt-1" for="password-new">New Password</label>
              </div>
              <div class="col col-md-4">
                <input type="password" class="form-control" name="password-new" required>
              </div>
            </div>
            <div class="form-group row">
              <div class="col col-md-3">
                <label class="pt-1" for="password-confirmation">Confirm Password</label>
              </div>
              <div class="col col-md-4">
                <input type="password" class="form-control" name="password-confirmation" required>
              </div>
            </div>
            <div class="form-group row pt-3">
              <div class="col col-md-3">
              </div>
              <div class="col col-md-4 text-right">
                <button type="submit" class="btn btn-success">
                  {{ __('Update') }}
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>


    </div>
  </div>
  @endsection

  @section('scriptPartialMerchant')
  @include('users._partial._merchant-js')
  @endsection