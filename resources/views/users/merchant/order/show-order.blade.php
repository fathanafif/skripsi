@extends('layouts.merchant-app')

@section('content')
<div class="ecommerce-widget">
    
    <!-- Content -->
    <div class="row">
        <div class="card col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 px-3 pb-5">

            {{-- SPECIFICATION --}}
            <div class="row pb-4 pt-5 px-3">
                <div class="col col-md-3 px-3 card-img-top">
                    <img class="card-img img-fluid text-center" src="{{ $order->service->image == null ? asset('assets/noThumbnail.png') : asset('storage/' . $order->service->image) }}" alt="">
                    @if (substr($order->file, -3) == "psd" || substr($order->file, -3) == ".ai" || substr($order->file, -3) == "cdr" || substr($order->file, -3) == "pdf" )
                        <div class="py-2">
                            <div class="row col-md-10 offset-md-1 justify-content-around">
                                <img class="" src="{{ $order->thumbnail }}" width="28">
                                <a href="{{ asset('storage') . "/" . $order->file }}" download="{{ $order->file }}"><button type="button" class="btn btn-xs btn-purple px-2">Download file<i class="icon-download1 pl-2"></i> </button></a>
                            </div>
                        </div>
                    @else
                        <div class="py-2">
                            <div class="row col-md-8 offset-md-2 justify-content-around">
                                <a href="{{ $order->file }}" target="_blank"><i class="icon-link pt-1 pr-2"></i>Visit file link</a>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="col col-md-3 px-auto">
                    <div>Order Code:</div>
                    <div class="order-code"><h4 class="mb-3"><b>{{ $order->order_code }}</b></h4></div>
                    <div class="mb-3">
                        <div>Info Customer:</div>
                        <div id="customerName">
                            <i class="icon-user pr-2"></i> {{ $order->customer->name }}
                        </div>
                        <div id="phoneNumber">
                            <i class="icon-phone pr-2"></i> {{ "+62" . $order->customer->phone_number }}
                        </div>
                        <div id="bankAccount">
                            <i class="icon-credit-card pr-2"></i> {{ $order->customer->account_number ." (" . $order->customer->real_bank_name . ")" }}
                            <div class="pl-4 ml-1" id="accountName">a/n {{ $order->customer->account_name }}</div>
                        </div>
                    </div>
                    <div class="order-date">Tanggal Order:</div>
                    <div><p><i class="icon-clock1 pr-2"></i> {{ $order->created_at->format('H:i') }} WIB</p></div>
                    <div><p><i class="icon-calendar1 pr-2"></i> {{ $order->created_at->format('d M Y') }}</p></div>
                </div>
                <div class="col col-md-3 px-auto">
                    <div>Jenis Cetak:</div>
                    <div id="serviceName"><h4><b>{{ $order->material->service->real_name }}</b></h4></div>
                    <div>Bahan:</div>
                    <div><h4 class="mb-0"><b id="materialName">{{ $order->material->real_material_name }}</b></h4></div>
                    <div id="materialSize">{{ $order->material->real_material_size . " " . $order->material->gramatur . " gsm" }}</div>
                    <div id="hargaSatuan">Rp {{ number_format($order->material->price,0,',','.') }}/lembar</div>
                    <div id="copies">{{ $order->copies }} rangkap</div>
                </div>
                <div class="col col-md-3 px-auto">
                    <div>Total Biaya:</div>
                    <div id="totalBiaya"><h4><b>Rp {{ number_format($order->total_cost,0,',','.') }}</b></h4></div>
                    <div>Pengambilan Barang:</div>
                    <div>
                        @if ($order->pick_up == 0)
                            Kirim ke lokasi customer<a href="{{ route('customer_location', $order->order_code) }}" target="_blank"><i class="icon-map-pin pl-2 pt-1" data-toggle="tooltip" data-placement="top" title="Lihat lokasi pengiriman"></i></a>
                            <small class="row font-weight-bold pl-3">{{ $order->pick_up == 0 ? "Biaya pengiriman dibebankan kepada customer." : "" }}</small>
                        @else
                        Customer mengambil barang ke percetakan<br>
                        @endif
                    </div>
                    <div class="mt-4">Status Order:</div>
                    <div class="mb-4 order-status">
                        @include('users._partial.merchant_status_order')
                    </div>
                </div>
            </div>
            
            {{-- ORDER NOTE --}}
            <div class="row offset-md-3">
                <div class="col-md-2">Catatan Order:</div>
            </div>
            <div class="row offset-md-3 pb-4">
                <div class="col font-weight-light order-note">{{ $order->order_note }}</div>
            </div>

            {{-- ORDER TRACK --}}
            @if ($order->copies > 1)
                @include('users._partial.merchant_track_order')
            @else
                @include('users._partial.merchant_track_order_without_proofing')
            @endif

            {{-- ACTION --}}
            @if ($order->copies > 1)
                <div class="row mx-3 py-3 border-top">
                    @if ($order->statuses->count() == 1)
                        <div class="row col-md-12 mx-0">
                            <form action="{{ route('store_status') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                                <div class="row mt-2">
                                    <div class="col-md-9">
                                        <label for="order_message">Message for customer</label>
                                        <textarea class="form-control" name="order_message" id="order_message" cols="100" rows="2" placeholder="Sampaikan pesan untuk customer (misal: lama waktu proofing atau antrian)"></textarea>
                                    </div>
                                    <div class="col-md-3 py-4 mt-3">
                                        <input type="text" name="order_id" value="{{ $order->id }}" hidden>
                                        <input class="btn btn-sm btn-link font-weight-bold" name="merchantConfirmation" type="submit" value="Decline"/>
                                        <button class="btn btn-sm btn-success font-weight-bold" name="merchantConfirmation" type="submit">Accept Order</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    @elseif ($order->lastStatus->title == "Confirmed by merchant")                
                        <div class="col col-md-6 offset-md-3 pt-5 px-auto">
                            <form action="{{ route('store_status') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="row pt-2 justify-content-around">
                                    <input type="text" name="order_id" value="{{ $order->id }}" hidden>
                                    <label class="my-auto align-middle pb-2" for="proofing_photo">Silahkan unggah foto hasil proofing Anda</label>
                                    <input class="form-group send-proofing" type="file" name="proofing_photo" id="custom-file-input" required>
                                    <button class="btn btn-sm btn-success font-weight-bold" name="sendProofing" type="submit">Send Proofing</button>
                                </div>
                            </form>
                        </div>
                    @elseif ($order->lastStatus->title == "Transfered")
                        <div class="col col-md-12 pt-3 px-auto">
                            <form action="{{ route('store_status') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="row py-2 justify-content-around">Apakah bukti pembayaran sudah sesuai dengan tagihan? Tekan "Process" jika sesuai.</div>
                                <div class="row pt-2 justify-content-around">
                                    <div class="justify-content-around">
                                        <input type="text" name="order_id" value="{{ $order->id }}" hidden>
                                        <input type="text" name="payment_verification" value="1" hidden>
                                        <button class="btn btn-sm btn-success font-weight-bold" name="process_order" type="submit">Process</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    @elseif ($order->lastStatus->title == "Slip is verified")
                        <div class="col col-md-12 pt-3 px-auto">
                            <form action="{{ route('store_status') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="row py-2 justify-content-around">Tekan tombol "Done" ketika order sudah selesai dikerjakan dan siap untuk diambil atau dikirim ke customer.</div>
                                <div class="row pt-2 justify-content-around">
                                    <input type="text" name="order_id" value="{{ $order->id }}" hidden>
                                    <input type="text" name="finished_order" value="1" hidden>
                                    <div class="justify-content-around">
                                            <button class="btn btn-sm btn-success font-weight-bold" name="process_order" type="submit">Done</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    @else
                    
                    @endif
                </div>
            @else
                <div class="row mx-3 py-3 border-top">
                    @if ($order->statuses->count() == 1)
                        <div class="row col-md-12 mx-0">
                            <form action="{{ route('store_status') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="row mt-2">
                                    <div class="col-md-9">
                                        <label for="order_message">Message for customer</label>
                                        <textarea class="form-control" name="order_message" id="order_message" cols="100" rows="2" placeholder="Sampaikan pesan untuk customer (misal: lama waktu proofing atau antrian)"></textarea>
                                    </div>
                                    <div class="col-md-3 py-4 mt-3">
                                        <input type="text" name="order_id" value="{{ $order->id }}" hidden>
                                        <input class="btn btn-sm btn-link font-weight-bold" name="merchantConfirmation" type="submit" value="Decline"/>
                                        <button class="btn btn-sm btn-success font-weight-bold" name="merchantConfirmation" type="submit">Accept Order</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    @elseif ($order->lastStatus->title == "Transfered")
                        <div class="col col-md-12 pt-3 px-auto">
                            <form action="{{ route('store_status') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="row py-2 justify-content-around">Apakah bukti pembayaran sudah sesuai dengan tagihan? Tekan "Process" jika sesuai.</div>
                                <div class="row pt-2 justify-content-around">
                                    <div class="justify-content-around">
                                        <input type="text" name="order_id" value="{{ $order->id }}" hidden>
                                        <input type="text" name="payment_verification" value="1" hidden>
                                        <button class="btn btn-sm btn-success font-weight-bold" name="process_order" type="submit">Process</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    @elseif ($order->lastStatus->title == "Slip is verified")
                        <div class="col col-md-12 pt-3 px-auto">
                            <form action="{{ route('store_status') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="row py-2 justify-content-around">Tekan tombol "Done" ketika order sudah selesai dikerjakan dan siap untuk diambil atau dikirim ke customer.</div>
                                <div class="row pt-2 justify-content-around">
                                    <input type="text" name="order_id" value="{{ $order->id }}" hidden>
                                    <input type="text" name="finished_order" value="1" hidden>
                                    <div class="justify-content-around">
                                            <button class="btn btn-sm btn-success font-weight-bold" name="process_order" type="submit">Done</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    @else
                    @endif
                </div>
            @endif
    </div>

@endsection

@section('scriptPartialMerchant')
@include('users._partial._merchant-js')
@endsection