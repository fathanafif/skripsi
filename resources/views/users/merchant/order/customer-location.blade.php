@extends('layouts.customer-location-merchant-app')

@section('content')
    <div class="colm-md-12" ng-controller="customerLocationForMerchant">
        <div class="gmaps" id="customerLocation"></div>
    </div>
@endsection

@section('scriptPartialMerchant')
    @include('users._partial._customer_location-js')
@endsection