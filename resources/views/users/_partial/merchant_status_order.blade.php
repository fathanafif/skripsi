@if ($order->copies > 1)    
    <div class="col-md-12 px-0">
        <h4 class="mb-0">
            @if ($order->lastStatus->title == "Pending")
                <b id="status">Pending</b>
            @elseif ($order->lastStatus->title == "Confirmed by merchant")
                <b id="status">{{ $order->lastStatus->title }}</b>
            @elseif ($order->lastStatus->title == "Proofing")
                <b id="status">Proofing is sent to customer</b>
            @elseif ($order->lastStatus->title == "Confirmed by customer")
                <b id="status">Waiting for payment</b>
            @elseif ($order->lastStatus->title == "Waiting for Payment Slip")
                <b id="status">Waiting for </b>
            @elseif ($order->lastStatus->title == "Transfered")
                <b id="status">Transfered</b>
            @elseif ($order->lastStatus->title == "Slip is verified")
                <b id="status">Overall print is on going</b>
            @elseif ($order->lastStatus->title == "Order is ready for pick up")
                <b id="status">Order is ready for pick up</b>
            @else
                Message Error!
            @endif
        </h4>
    </div>
    <div class="px-3">
        @if ($order->lastStatus->title == "Pending")
            <small class="row font-weight-bold">Customer menunggu Anda mengkonfirmasi order.</small>
        @elseif ($order->lastStatus->title == "Confirmed by merchant")
            <small class="row font-weight-bold">Silahkan melakukan proofing order.</small>
        @elseif ($order->lastStatus->title == "Customer's Confirmation")
            <small class="row font-weight-bold">Silahkan konfirmasi order berdasarkan proofing yang telah Anda terima.</small>
        @elseif ($order->lastStatus->title == "Proofing")
            <small class="row font-weight-bold">Menunggu konfirmasi customer terkait proofing order yang telah Anda lakukan.</small>
        @elseif ($order->lastStatus->title == "Confirmed by customer")
            <small class="row font-weight-bold">Menunggu customer melakukan pembayaran.</small>
        @elseif ($order->lastStatus->title == "Waiting for Payment Slip")
            <small class="row font-weight-bold">Silahkan unggah bukti pembayaran untuk melanjutkan order.</small>
        @elseif ($order->lastStatus->title == "Slip is verified")
            <small class="row font-weight-bold">Order ini sedang dalam proses pengerjaan.</small>
        @elseif ($order->lastStatus->title == "Order is ready for pick up")
            <small class="row font-weight-bold">
                @if ($order->pick_up == 1)
                    Order sudah dapat diambil.
                @else
                    Order dalam tahap pengiriman.
                @endif
            </small>
        @else
            <small class="row font-weight-bold">Menunggu konfirmasi merchant (terkait bukti pembayaran).</small>
        @endif
    </div>
@else
    <div>
        <h4 class="mb-0">
            @if ($order->lastStatus->title == "Pending")
                <b id="status">Pending</b>
            @elseif ($order->lastStatus->title == "Confirmed by merchant")
                <b id="status">Waiting for payment</b>
            @elseif ($order->lastStatus->title == "Waiting for Payment Slip")
                <b id="status">Waiting for payment</b>
            @elseif ($order->lastStatus->title == "Transfered")
                <b id="status">Transfered</b>
            @elseif ($order->lastStatus->title == "Slip is verified")
                <b id="status">Overall print is on going</b>
            @elseif ($order->lastStatus->title == "Order is ready for pick up")
                <b id="status">Order is ready for pick up</b>
            @else
                Message Error!
            @endif
        </h4>
    </div>
    <div class="px-3">
        @if ($order->lastStatus->title == "Pending")
            <small class="row font-weight-bold">Customer menunggu Anda mengkonfirmasi order.</small>
        @elseif ($order->lastStatus->title == "Confirmed by merchant")
            <small class="row font-weight-bold">Menunggu customer melakukan pembayaran.</small>
        @elseif ($order->lastStatus->title == "Customer's Confirmation")
            <small class="row font-weight-bold">Silahkan konfirmasi order berdasarkan proofing yang telah Anda terima.</small>
        @elseif ($order->lastStatus->title == "Proofing")
            <small class="row font-weight-bold">Hasil cetak sudah dapat Anda lihat.</small>
        @elseif ($order->lastStatus->title == "Confirmed by customer")
            <small class="row font-weight-bold">Silahkan melakukan pembayaran untuk melanjutkan order.</small>
        @elseif ($order->lastStatus->title == "Waiting for Payment Slip")
            <small class="row font-weight-bold">Silahkan unggah bukti pembayaran untuk melanjutkan order.</small>
        @elseif ($order->lastStatus->title == "Slip is verified")
            <small class="row font-weight-bold">Order ini sedang dalam proses pengerjaan.</small>
        @elseif ($order->lastStatus->title == "Order is ready for pick up")
            <small class="row font-weight-bold">
                @if ($order->pick_up == 1)
                    Order sudah dapat diambil.
                @else
                    Order dalam tahap pengiriman.
                @endif
            </small>
        @else
            <small class="row font-weight-bold">Menunggu konfirmasi merchant (terkait bukti pembayaran).</small>
        @endif
    </div>
@endif