<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="{{ asset('userAssets') }}/assets/vendor/jquery/jquery-3.3.1.min.js"></script>
<script src="{{ asset('userAssets') }}/assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
<script src="{{ asset('userAssets') }}/assets/vendor/slimscroll/jquery.slimscroll.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.8/angular.min.js"></script>

<script type="text/javascript">
  var app = angular.module('angularJS', []);
  app.controller('sidebar', function($scope, $http){
    $http({
      method: 'GET',
      url: '{{ route('jsonCountService') }}'
    }).then(function(data){
      $scope.countService = data.data;
      // console.log($scope.count);
    }),
    $http({
      method: 'GET',
      url: '{{ route('jsonCountOrder') }}'
    }).then(function(data){
      $scope.countOrder = data.data;
      // console.log($scope.count);
    })
    
    $(document).ready(function($) {
      var path = window.location.pathname;
      console.log(path);
      if (path == '/account/edit') {
        $('.account').addClass('active');
      } else {
        $('.account').addClass('active');
      };
    });
  });
  
  app.controller('merchantAccount', function($scope, $http) {
    var mapOptions = {
      center: ["{{ $me->lat }}", "{{ $me->long }}"],
      zoom: 11
    }
    // Creating a map object
    var map = new L.map('map', mapOptions);
    // Creating a Layer object
    var layer = new L.TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
    // Adding layer to the map
    map.addLayer(layer);
    // Creating a marker
    var m_marker = L.icon({
      iconUrl: "{{ asset('assets/m_marker.png') }}",
      iconAnchor: [18, 50],
      iconSize: [34, 50],
      popupAnchor: [0, -50]
    });
    var lat = "{{ $me->lat }}";
    var lng = "{{ $me->long }}";
    // var lng = parseFloat(document.getElementById("lng"));
    L.marker([ lat, lng], {icon: m_marker}).addTo(map);
  });
  
  app.controller('editAccount', function($scope, $http) {
    var mapOptions = {
      center: [-7.782905, 110.367048],
      zoom: 12
    }
    // Creating a map object
    var map = new L.map('map', mapOptions);
    // Creating a Layer object
    var layer = new L.TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
    // Adding layer to the map
    map.addLayer(layer);
    
    var m_marker = null,
    c_icon = new L.icon({
      iconUrl: "{{ asset('assets/m_marker.png') }}",
      iconAnchor: [18, 50],
      iconSize: [34, 50],
      popupAnchor: [0, -50]
    });
    
    document.getElementById("lat").style.display = "none";
    document.getElementById("long").style.display = "none";
    
    map.on('click', function (e) {
      if (m_marker !== null) {
        map.removeLayer(m_marker);
      }
      m_marker = L.marker(e.latlng, {icon: c_icon}).addTo(map).bindPopup(e.latlng.lat + ", " + e.latlng.lng);
      document.getElementById("lat").value = e.latlng.lat;
      document.getElementById("long").value = e.latlng.lng;
    });
    
    $("#cancelUpload").click(function(){
      document.getElementById("custom-file-input").value = "";   
    })

    $("#phoneNumber").on("keypress",function (event) {
      $(this).val($(this).val().replace(/[^\d].+/, ""));
      if ((event.which < 48 || event.which > 57)) {
        event.preventDefault();
      }
    });
    
    $("#account_number").on("keypress",function (event) {
      $(this).val($(this).val().replace(/[^\d].+/, ""));
      if ((event.which < 48 || event.which > 57)) {
        event.preventDefault();
      }
    });
    
    $("#copies").on("keypress",function (event) {
      $(this).val($(this).val().replace(/[^\d].+/, ""));
      $("#copies").change(function() {
        $("#account_number").attr('maxlength', '10');
      });
      if ((event.which < 48 || event.which > 57)) {
        event.preventDefault();
      }
    });
    
    $("#bank_names").change(function() {
      if ($("#bank_names").val() === "mandiri") {
        $("#account_number").val('');
        $("#account_number").attr('maxlength', '13');
      } else if ($(this).val() == 'bni' || $(this).val() == 'bca') {
        $("#account_number").val('');
        $("#account_number").attr('maxlength', '10');
      } else {
        $("#account_number").val('');
        $("#account_number").attr('maxlength', '15');
      }
    });
    
    $("#bank_names").change(function() {
      $("account_number").val("");
    });
  });
</script>