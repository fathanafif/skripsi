@if ($order->copies > 1)
    <div class="col-md-12 px-0">
        <h4 class="my-0">
            @if ($order->lastStatus->title == "Pending")
                <b id="status">Pending</b>
            @elseif ($order->lastStatus->title == "Confirmed by merchant")
                <b id="status">{{ $order->lastStatus->title }}</b>
            @elseif ($order->lastStatus->title == "Proofing")
                <b id="status">Proofing is ready</b>
            @elseif ($order->lastStatus->title == "Confirmed by customer")
                <b id="status">Waiting for your payment</b>
            @elseif ($order->lastStatus->title == "Waiting for Payment Slip")
                <b id="status">Waiting for your payment slip</b>
            @elseif ($order->lastStatus->title == "Transfered")
                <b id="status">Transfered</b>
            @elseif ($order->lastStatus->title == "Slip is verified")
                <b id="status">Overall print is on going</b>
            @elseif ($order->lastStatus->title == "Order is ready for pick up")
                <b id="status">Order is ready for pick up</b>
            @else
                Message Error!
            @endif
        </h4>
    </div>
    <div class="px-3">
        @if ($order->lastStatus->title == "Pending")
            <small class="row font-weight-bold">Menunggu konfirmasi pihak percetakan (merchant).</small>
        @elseif ($order->lastStatus->title == "Confirmed by merchant")
            <small class="row font-weight-bold">Menunggu merchant melakukan proofing order.</small>
        @elseif ($order->lastStatus->title == "Proofing")
            <small class="row font-weight-bold">Silahkan konfirmasi proofing yang telah dilakukan merchant.</small>
        @elseif ($order->lastStatus->title == "Confirmed by customer")
            <small class="row font-weight-bold">Silahkan melakukan pembayaran agar order dapat dilanjutkan.</small>
        @elseif ($order->lastStatus->title == "Waiting for Payment Slip")
            <small class="row font-weight-bold">Silahkan unggah bukti pembayaran untuk melanjutkan order.</small>
        @elseif ($order->lastStatus->title == "Transfered")
            <small class="row font-weight-bold">Menunggu merchant melakukan verifikasi pembayaran yang telah Anda lakukan.</small>
        @elseif ($order->lastStatus->title == "Slip is verified")
            <small class="row font-weight-bold">Order Anda sedang dalam proses pengerjaan.</small>
        @elseif ($order->lastStatus->title == "Order is ready for pick up")
            <small class="row font-weight-bold">
                @if ($order->pick_up == 1)
                    Order Anda sudah dapat diambil.
                @else
                    Order Anda dalam tahap pengiriman.
                @endif
            </small>
        @else
        <small class="row font-weight-bold">Message Error!</small>
        @endif
    </div>
@else
    <div class="col-md-12 px-0">
        <h4 class="my-0">
            @if ($order->lastStatus->title == "Pending")
                <b id="status">Pending</b>
            @elseif ($order->lastStatus->title == "Confirmed by merchant")
                <b id="status">{{ $order->lastStatus->title }}</b>
            @elseif ($order->lastStatus->title == "Proofing")
                <b id="status">Proofing is ready</b>
            @elseif ($order->lastStatus->title == "Confirmed by customer")
                <b id="status">Waiting for your payment</b>
            @elseif ($order->lastStatus->title == "Waiting for Payment Slip")
                <b id="status">Waiting for your payment slip</b>
            @elseif ($order->lastStatus->title == "Transfered")
                <b id="status">Transfered</b>
            @elseif ($order->lastStatus->title == "Slip is verified")
                <b id="status">Overall print is on going</b>
            @elseif ($order->lastStatus->title == "Order is ready for pick up")
                <b id="status">Order is ready for pick up</b>
            @else
                Message Error!
            @endif
        </h4>
    </div>
    <div class="px-3">
        @if ($order->lastStatus->title == "Pending")
            <small class="row font-weight-bold">Menunggu konfirmasi pihak percetakan (merchant).</small>
        @elseif ($order->lastStatus->title == "Confirmed by merchant")
            <small class="row font-weight-bold">Silahkan melakukan pembayaran</small>
        @elseif ($order->lastStatus->title == "Waiting for Payment Slip")
            <small class="row font-weight-bold">Silahkan unggah bukti pembayaran untuk melanjutkan order.</small>
        @elseif ($order->lastStatus->title == "Slip is verified")
            <small class="row font-weight-bold">Order Anda sedang dalam proses pengerjaan.</small>
        @elseif ($order->lastStatus->title == "Order is ready for pick up")
            <small class="row font-weight-bold">
                @if ($order->pick_up == 1)
                    Order Anda sudah dapat diambil.
                @else
                    Order Anda dalam tahap pengiriman.
                @endif
            </small>
        @else
            <small class="row font-weight-bold">Menunggu konfirmasi merchant (terkait bukti pembayaran).</small>
        @endif
    </div>
@endif