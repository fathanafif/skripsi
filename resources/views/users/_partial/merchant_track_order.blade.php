{{-- ORDER TRACK --}}
<div class="row border-top justify-content-around pt-5 mx-3 text-center">
    @if ($order->lastStatus->title == "Pending")
        @for ($i = 0; $i < 7; $i++)
        <div class="col-md-1">
            <span class="roundedTracking"></span>
        </div>
        @endfor
    @elseif ($order->lastStatus->title == "Confirmed by merchant")
        @for ($i = 0; $i < 1; $i++)
        <div class="col-md-1">
            <span class="roundedTracking bg-success"><i class="icon-check text-light font-weight-bold statusCheck"></i></span>
        </div>
        @endfor
        @for ($i = 0; $i < 6; $i++)
        <div class="col-md-1">
            <span class="roundedTracking"></span>
        </div>
        @endfor
    @elseif ($order->lastStatus->title == "Proofing")
        @for ($i = 0; $i < 2; $i++)
        <div class="col-md-1">
            <span class="roundedTracking bg-success"><i class="icon-check text-light font-weight-bold statusCheck"></i></span>
        </div>
        @endfor
        @for ($i = 0; $i < 5; $i++)
        <div class="col-md-1">
            <span class="roundedTracking"></span>
        </div>
        @endfor
    @elseif ($order->lastStatus->title == "Confirmed by customer")
        @for ($i = 0; $i < 3; $i++)
        <div class="col-md-1">
            <span class="roundedTracking bg-success"><i class="icon-check text-light font-weight-bold statusCheck"></i></span>
        </div>
        @endfor
        @for ($i = 0; $i < 4; $i++)
        <div class="col-md-1">
            <span class="roundedTracking"></span>
        </div>
        @endfor
    @elseif ($order->lastStatus->title == "Transfered")
        @for ($i = 0; $i < 4; $i++)
        <div class="col-md-1">
            <span class="roundedTracking bg-success"><i class="icon-check text-light font-weight-bold statusCheck"></i></span>
        </div>
        @endfor
        @for ($i = 0; $i < 3; $i++)
        <div class="col-md-1">
            <span class="roundedTracking"></span>
        </div>
        @endfor
    @elseif ($order->lastStatus->title == "Slip is verified")
        @for ($i = 0; $i < 5; $i++)
        <div class="col-md-1">
            <span class="roundedTracking bg-success"><i class="icon-check text-light font-weight-bold statusCheck"></i></span>
        </div>
        @endfor
        @for ($i = 0; $i < 2; $i++)
        <div class="col-md-1">
            <span class="roundedTracking"></span>
        </div>
        @endfor
    @elseif ($order->lastStatus->title == "Order is ready for pick up")
        @for ($i = 0; $i < 6; $i++)
        <div class="col-md-1">
            <span class="roundedTracking bg-success"><i class="icon-check text-light font-weight-bold statusCheck"></i></span>
        </div>
        @endfor
        @for ($i = 0; $i < 1; $i++)
        <div class="col-md-1">
            <span class="roundedTracking"></span>
        </div>
        @endfor
    @else
        @for ($i = 0; $i < 7; $i++)
        <div class="col-md-1">
            <span class="roundedTracking bg-success"><i class="icon-check text-light font-weight-bold statusCheck"></i></span>
        </div>
        @endfor
    @endif
</div>

{{-- TRACK LINE --}}
<div class="track-order-progress-bar">
    <div class="progress progress-xs" id="trackOrderLine">
        @if ($order->lastStatus->title == "Pending")
            <div class="progress-bar" role="progressbar" style="width: 0%" aria-valuemin="0" aria-valuemax="100"></div>
        @elseif ($order->lastStatus->title == "Confirmed by merchant")
            <div class="progress-bar" role="progressbar" style="width: 17%" aria-valuemin="0" aria-valuemax="100"></div>
        @elseif ($order->lastStatus->title == "Proofing")
            <div class="progress-bar" role="progressbar" style="width: 33.5%" aria-valuemin="0" aria-valuemax="100"></div>
        @elseif ($order->lastStatus->title == "Confirmed by customer")
            <div class="progress-bar" role="progressbar" style="width: 50%" aria-valuemin="0" aria-valuemax="100"></div>
        @elseif ($order->lastStatus->title == "Transfered")
            <div class="progress-bar" role="progressbar" style="width: 66.7%" aria-valuemin="0" aria-valuemax="100"></div>
        @elseif ($order->lastStatus->title == "Slip is verified")
            <div class="progress-bar" role="progressbar" style="width: 83.25%" aria-valuemin="0" aria-valuemax="100"></div>
        @elseif ($order->lastStatus->title == "Order is ready for pick up")
            <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuemin="0" aria-valuemax="100"></div>
        @else
        @endif
    </div>
</div>

{{-- TITLE --}}
<div class="row justify-content-around pt-2 pb-3 text-center mx-3">
    <div class="col-md-1 trackTitle"><small class="font-weight-bold">Merchant confirmation</small></div>
    <div class="col-md-1 trackTitle"><small class="font-weight-bold">Proofing</small></div>
    <div class="col-md-1 trackTitle"><small class="font-weight-bold">Customer confirmation</small></div>
    <div class="col-md-1 trackTitle"><small class="font-weight-bold">Payment</small></div>
    <div class="col-md-1 trackTitle"><small class="font-weight-bold">Payment verification</small></div>
    <div class="col-md-1 trackTitle"><small class="font-weight-bold">Processing<br>order</small></div>
    <div class="col-md-1 trackTitle"><small class="font-weight-bold">Success</small></div>
</div>

{{-- KETERANGAN --}}
<div class="row pb-3 text-center mx-3">
    @foreach ($order->statuses as $item)
        @if (count($order->statuses) == 1)
        <div class="col col-md-12 py-4">"Customer menunggu Anda mengkonfirmasi order."</div>
        @else
            @if ($loop->first) 
            @else
                <div class="col-md-1 keterangan px-0">
                    <small class="font-weight-bold">{{ $item->created_at->format('H:i') }} WIB</small>
                    <small class="font-weight-bold">{{ $item->created_at->format('d/m/y') }}</small>
                    <div class="col-md-12 px-0">
                        <small class="font-weight-bold">{{ $item->message }}</small>
                    </div>
                </div>
            @endif
        @endif
    @endforeach
</div>

@if ($order->statuses->count() >= 5)
    @if ($order->statuses[4]->payment != null)
        <div class="row pb-5 text-center mx-3">
            <div class="col-md-1 px-0 trackImage"></div>
            <div class="col-md-1 px-0 trackImage">
                <a href="{{ url('/storage/'. $order->statuses[2]->proofing) }}" target="_blank">
                    <img class="img-fluid trackOrder" id="proofing-image" src="{{ asset('storage/' . $order->statuses[2]->proofing) }}" alt="">
                </a>
            </div>
            <div class="col-md-1 px-0 trackImage"></div>
            <div class="col-md-1 px-0 trackImage">
                <a href="{{ url('/storage/'. $order->statuses[4]->payment) }}" target="_blank">
                    <img class="img-fluid trackOrder" id="payment-slip" src="{{ asset('storage/' . $order->statuses[4]->payment) }}" alt="">
                </a>
            </div>
        </div>
        @else
    @endif
@elseif ($order->statuses->count() >= 3)
    @if ($order->statuses[2]->proofing != null)
    <div class="row pb-5 text-center mx-3">
        <div class="col-md-1 px-0 trackImage"></div>
        <div class="col-md-1 px-0 trackImage">
            <a href="{{ url('/storage/'. $order->statuses[2]->proofing) }}" target="_blank">
                <img class="img-fluid trackOrder" id="proofing-image" src="{{ asset('storage/' . $order->statuses[2]->proofing) }}" alt="">
            </a>
        </div>
    </div>
    @else
    @endif
@else
    <div class="row pb-3"></div>
@endif