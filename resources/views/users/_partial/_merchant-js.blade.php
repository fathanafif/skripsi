<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="{{ asset('userAssets') }}/assets/vendor/jquery/jquery-3.3.1.min.js"></script>
<script src="{{ asset('userAssets') }}/assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
<script src="{{ asset('userAssets') }}/assets/vendor/slimscroll/jquery.slimscroll.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.8/angular.min.js"></script>

<script type="text/javascript">
  var app = angular.module('angularJS', []);
  app.controller('sidebar', function($scope, $http){
    $http({
      method: 'GET',
      url: '{{ route('jsonCountService') }}'
    }).then(function(data){
      $scope.countService = data.data;
      // console.log($scope.count);
    }),
    $http({
      method: 'GET',
      url: '{{ route('jsonCountOrder') }}'
    }).then(function(data){
      $scope.countOrder = data.data;
      // console.log($scope.count);
    })
    $(document).ready(function($) {
      var path = window.location.pathname;
      var page = window.location.pathname.split("/").pop();
      console.log(path);
      if (path == '/my_services' || path == '/service/' + page) {
        $('.service').addClass('active');
      } else if (path == '/order/' + page || path == path) {
        $('.order').addClass('active');
      } else {
        $('.home').addClass('active');
      };
    });
  });
  
  $("#estimatedTime").on("keypress",function (event) {
    $(this).val($(this).val().replace(/[^\d].+/, ""));
    if ((event.which < 48 || event.which > 57)) {
      event.preventDefault();
    }
    $("#estimatedTime").attr('minlength', '1');
    $("#estimatedTime").attr('maxlength', '2');
  });
  
  $("#gramatur").on("keypress",function (event) {
    $(this).val($(this).val().replace(/[^\d].+/, ""));
    if ((event.which < 48 || event.which > 57)) {
      event.preventDefault();
    }
    $("#gramatur").attr('minlength', '2');
    $("#gramatur").attr('maxlength', '3');
  });
  
  $("#price").on("keypress",function (event) {
    $(this).val($(this).val().replace(/[^\d].+/, ""));
    if ((event.which < 48 || event.which > 57)) {
      event.preventDefault();
    }
    $("#price").attr('minlength', '3');
    $("#price").attr('maxlength', '5');
  });
  
  $(document).ready(function(){
    var maxField = 10; //Input fields increment limitation
    var addButton = $('#add_button'); //Add button selector
    var wrapper = $('#field_wrapper'); //Input field wrapper
    var x = 1; //Initial field counter is 1
    var inject = 
    //Once add button is clicked
    $(addButton).click(function(){
      if(x < maxField){
        $.get('{{ route('jsonMaterials') }}')
        .done (function (data) {  
          console.log(data);
          var partOne =
          `
          <div>
            <div class="form-group row">
              <div class="col col-md-3 input-group">
                <select class="form-control" name="materialName[]" id="materialName" required>
                  `;
                  var partTwo = '<option value="">Nama bahan</option>';
                  $.each(data.material_names, function(k, v) {
                    partTwo = partTwo + '<option value="' + k + '">' + v + '</option>'
                  });
                  
                  var partThree = 
                  `
                </select>
              </div>
              <div class="col col-md-2 input-group">
                <input type="text" class="form-control" id="gramatur" name="gramatur[]" placeholder="Gramatur" required>
              </div>
              <div class="col col-md-3 input-group">
                <select class="form-control" name="materialSize[]" id="materialSizeInput" required>
                  `;
                  
                  var partFour = '<option value="">Ukuran bahan</option>';
                  $.each(data.material_sizes, function(k, v) {
                    partFour = partFour + '<option value="' + k + '">' + v + '</option>'
                  });
                  
                  var partFive =
                  `
                </select>
              </div>
              <div class="col col-md-3 input-group">
                <div class="input-group-prepend"><span class="input-group-text">Rp</span></div>
                <input type="text" class="form-control" id="price" placeholder="Harga" name="price[]" required>
              </div>
              <a id="remove_button">
                <i class="icon-trash-2"></i>
              </a>
            </div>
          </div>
          `;
          fieldHTML = partOne + partTwo + partThree + partFour + partFive
          $(wrapper).append(fieldHTML); //Add field html
        });
        x++;
      }
    });
    
    //Once remove button is clicked
    $(wrapper).on('click', '#remove_button', function(e){
      e.preventDefault();
      $(this).parent("div").remove(); //Remove field html
      x--; //Decrement field counter
    });
    
    $("#cancelUpload").click(function(){
      document.getElementById("custom-file-input").value = "";   
    })
  });
</script>

{{-- <script>
</script> --}}