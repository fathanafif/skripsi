<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="{{ asset('userAssets') }}/assets/vendor/jquery/jquery-3.3.1.min.js"></script>
<script src="{{ asset('userAssets') }}/assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
<script src="{{ asset('userAssets') }}/assets/vendor/slimscroll/jquery.slimscroll.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.8/angular.min.js"></script>

<script>
    function initMap() {
        var lat = document.getElementById('lat').value;
        var lng = document.getElementById('lng').value;
        var myLatLng = {
            lat: parseFloat(lat),
            lng: parseFloat(lng)
        };
        console.log(lat)
        var map = new google.maps.Map(document.getElementById('mapMerchant'), {
          zoom: 12,
          center: myLatLng
        });
        var marker = new google.maps.Marker({
        position: {
            lat: parseFloat(lat),
            lng: parseFloat(lng)
        },
        map: map,
        icon: {
          url: "{{ asset('assets/m_marker.png') }}",
          scaledSize: new google.maps.Size(30, 44),
        }
      });
      }
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBYVnhqA-HEUILySThn8znnQ_wrE0Q2Cuw&callback=initMap" async defer></script>