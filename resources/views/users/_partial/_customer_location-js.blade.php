<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="{{ asset('userAssets') }}/assets/vendor/jquery/jquery-3.3.1.min.js"></script>
<script src="{{ asset('userAssets') }}/assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
<script src="{{ asset('userAssets') }}/assets/vendor/slimscroll/jquery.slimscroll.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.8/angular.min.js"></script>

<script type="text/javascript">
  var app = angular.module('angularJS', []);  
  app.controller('customerLocationForMerchant', function($scope, $http) {
    var mapOptions = {
      center: [-7.782905, 110.367048],
      zoom: 12
    }
    // Creating a map object
    var map = new L.map('customerLocation', mapOptions);
    // Creating a Layer object
    var layer = new L.TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
    // Adding layer to the map
    map.addLayer(layer);
    // Creating a marker
    var m_marker = L.icon({
      iconUrl: "{{ asset('assets/m_marker.png') }}",
      iconAnchor: [18, 50],
      iconSize: [34, 50],
      popupAnchor: [0, -50]
    });
    
    var c_marker = L.icon({
      iconUrl: "{{ asset('assets/c_marker.png') }}",
      iconAnchor: [18, 50],
      iconSize: [34, 50],
      popupAnchor: [0, -50]
    });

    var m_lat = "{{ $order->service->user->lat }}";
    var m_lng = "{{ $order->service->user->long }}";
    
    var c_lat = "{{ $order->customer->lat }}";
    var c_lng = "{{ $order->customer->long }}";
    // var lng = parseFloat(document.getElementById("lng"));

    L.marker([ m_lat, m_lng], {icon: m_marker}).addTo(map);
    L.marker([ c_lat, c_lng], {icon: c_marker}).addTo(map);
  });
  
  
</script>