<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="{{ asset('userAssets') }}/assets/vendor/jquery/jquery-3.3.1.min.js"></script>
<script src="{{ asset('userAssets') }}/assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
<script src="{{ asset('userAssets') }}/assets/vendor/slimscroll/jquery.slimscroll.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.8/angular.min.js"></script>

<script type="text/javascript">
  var app = angular.module('angularJS', []);
  app.controller('indexService', function($scope, $http){
    $http({
      method: 'GET',
      url: '{{ route('jsonExploreService') }}'
    }).then(function(data){
      $scope.dataService = data.data;
      // console.log($scope.dataService);
    });
  });
  
  app.controller('sidebar', function($scope, $http){
    $(document).ready(function($) {
      var path = window.location.pathname;
      var page = window.location.pathname.split("/").pop();
      if (path == '/explore' || path == '/service/' + page) {
        $('.explore').addClass('active');
      } else if (path == '/order/' + page || path == path) {
        $('.order').addClass('active');
      } else {
        $('.explore').addClass('active');
      };
    });
  });

  app.controller('createOrder', function($scope, $http){
    $("#phoneNumber").on("keypress",function (event) {
      $(this).val($(this).val().replace(/[^\d].+/, ""));
      if ((event.which < 48 || event.which > 57)) {
        event.preventDefault();
      }
    });
    $("#cancelUpload").click(function(){
      document.getElementById("custom-file-input").value = "";   
    })
  });
  
  
</script>

{{-- <script>
</script> --}}