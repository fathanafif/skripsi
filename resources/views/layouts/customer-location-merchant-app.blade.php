<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <!-- Required meta tags -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <meta charset="utf-8">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
  <script data-require="angular.js@1.6.5" data-semver="1.6.5" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular.min.js"></script>
  <script data-require="angular-animate@1.6.*" data-semver="1.6.5" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular-animate.min.js"></script>
  <script data-require="angular-touch@1.6.*" data-semver="1.6.2" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.2/angular-touch.js"></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  <link rel="stylesheet" href="{{ asset('userAssets') }}/assets/vendor/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="{{ asset('userAssets') }}/assets/libs/css/style.css">
  <link rel="stylesheet" href="{{ asset('userAssets') }}/assets/vendor/charts/c3charts/c3.css">
  <link rel="stylesheet" href="{{ asset('userAssets') }}/assets/vendor/fonts/icomoon/style.css">
  <link rel="stylesheet" href="https://d19vzq90twjlae.cloudfront.net/leaflet/v0.7.7/leaflet.css" />
	<script src="https://d19vzq90twjlae.cloudfront.net/leaflet/v0.7.7/leaflet.js"></script>
  <title>PrintOn</title>
</head>
<body class="px-0" ng-app="angularJS">
  <div class="dashboard-main-wrapper">

    <!-- header -->
    <div class="dashboard-header">
      <nav class="navbar navbar-expand-lg bg-white fixed-top">
        <a class="navbar-brand" href="/">PrintOn</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto navbar-right-top">
            <li class="nav-item">
              <a class="nav-link nav-icons" href="{{ route('create_service') }}" id="navbarDropdownMenuLink1" aria-haspopup="true" aria-expanded="false"><i class="icon-plus-circle" data-toggle="tooltip" data-placement="bottom" title="Create New Service"></i></a>
            </li>
            <div class="my-auto ml-4">{{ Auth::user()->name }}</div>
            <li class="nav-item dropdown nav-user">
              <a class="nav-link nav-user-img" href="#" id="navbarDropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{ Auth::user()->profilePhoto == NULL ? asset('assets/blankHead.png') : asset('storage/' . Auth::user()->profilePhoto) }}" alt="" class="user-avatar-md rounded-circle"></a>
              <div class="dropdown-menu dropdown-menu-right nav-user-dropdown" aria-labelledby="navbarDropdownMenuLink2">
                <div class="nav-user-info">
                  <h5 class="mb-0 text-dark nav-user-name">{{ Auth::user()->name }}</h5>
                  <span class="status"></span><span class="ml-2">{{ Auth::user()->email }}</span>
                </div>
                <!-- <a class="dropdown-item" href="#"><i class="fas fa-user mr-2"></i>Account</a>
                <a class="dropdown-item" href="#"><i class="fas fa-cog mr-2"></i>Setting</a> -->
                <!-- Authentication Links -->
                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="icon-log-out"></i>
                  {{ __('Logout') }}
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
                </form>
              </div>
            </li>
          </ul>
        </div>
      </nav>
    </div>
    <!-- content + footer -->
    <div class="dashboard-wrapper-cl">
      <div class="dashboard-ecommerce">
        <!-- dashboard content -->
        <div class="container-fluid dashboard-content p-0">
          @yield('content')
        </div>
      </div>
    </div>
    <!-- footer -->
    <div class="footer align-bottom">
      <div class="container-fluid">
        <div class="row">
          <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
            Copyright © 2018 fathanafif & Concept. All rights reserved. Dashboard by <a href="https://colorlib.com/wp/">Colorlib</a>.
          </div>
          <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
            <div class="text-md-right footer-links d-none d-sm-block">
              <a href="javascript: void(0);">About</a>
              <a href="javascript: void(0);">Support</a>
              <a href="javascript: void(0);">Contact Us</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @yield('scriptPartialMerchant')
</body>
</html>
