<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
  <script data-require="angular.js@1.6.5" data-semver="1.6.5" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular.min.js"></script>
  <script data-require="angular-animate@1.6.*" data-semver="1.6.5" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular-animate.min.js"></script>
  <script data-require="angular-touch@1.6.*" data-semver="1.6.2" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.2/angular-touch.js"></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  <link rel="stylesheet" href="{{ asset('userAssets') }}/assets/vendor/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="{{ asset('userAssets') }}/assets/libs/css/style.css">
  <link rel="stylesheet" href="{{ asset('userAssets') }}/assets/vendor/charts/c3charts/c3.css">
  <link rel="stylesheet" href="{{ asset('userAssets') }}/assets/vendor/fonts/icomoon/style.css">
  <link rel="stylesheet" href="https://d19vzq90twjlae.cloudfront.net/leaflet/v0.7.7/leaflet.css" />
	<script src="https://d19vzq90twjlae.cloudfront.net/leaflet/v0.7.7/leaflet.js"></script>
  <title>PrintOn</title>
</head>
<body class="px-0" ng-app="angularJS">
  <div class="dashboard-main-wrapper">
    
    <!-- header -->
    <div class="dashboard-header">
      <nav class="navbar navbar-expand-lg bg-white fixed-top">
        <a class="navbar-brand" href="/">PrintOn</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto navbar-right-top">
            <div class="my-auto">{{ Auth::user()->name }}</div>
            <li class="nav-item dropdown nav-user">
              <a class="nav-link nav-user-img" href="#" id="navbarDropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{ Auth::user()->profilePhoto == NULL ? asset('assets/blankHead.png') : asset('storage/' . Auth::user()->profilePhoto) }}" alt="" class="user-avatar-md rounded-circle"></a>
              <div class="dropdown-menu dropdown-menu-right nav-user-dropdown" aria-labelledby="navbarDropdownMenuLink2">
                <div class="nav-user-info">
                  <h5 class="mb-0 text-dark nav-user-name">{{ Auth::user()->name }}</h5>
                  <span class="status"></span><span class="ml-2">{{ Auth::user()->email }}</span>
                </div>
                <!-- <a class="dropdown-item" href="#"><i class="fas fa-user mr-2"></i>Account</a>
                  <a class="dropdown-item" href="#"><i class="fas fa-cog mr-2"></i>Setting</a> -->
                  <!-- Authentication Links -->
                  <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="icon-log-out"></i>
                    {{ __('Logout') }}
                  </a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                  </form>
                </div>
              </li>
            </ul>
          </div>
        </nav>
      </div>
      <!-- sidebar -->
      <div class="nav-left-sidebar sidebar-light" ng-controller="sidebar">
        <div class="menu-list">
          <nav class="navbar navbar-expand-lg navbar-light">
            <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
              <ul class="navbar-nav flex-column">
                <li class="nav-divider">
                  Menu
                </li>
                <li class="nav-item">
                  <a class="nav-link explore" href="{{ route('explore') }}" aria-expanded="false" aria-controls="submenu-1"><i class="icon-compass"></i>Explore</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link order" href="{{ route('order') }}" aria-expanded="false" data-target="#submenu-1" aria-controls="submenu-1"><i class="icon-shopping-bag"></i>My Orders</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link account" href="" data-toggle="collapse" aria-expanded="false" data-target="#submenu-2" aria-controls="submenu-2"><i class="icon-user1 pr-1"></i>Account</a>
                  <div id="submenu-2" class="collapse submenu" style="">
                    <ul class="nav flex-column">
                      <li class="nav-item">
                        <a class="nav-link" href="{{ route('account') }}">My Account</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="">Security Setting</a>
                      </li>
                    </ul>
                  </div>
                </li>
              </ul>
            </div>
          </nav>
        </div>
      </div>
      <!-- content + footer -->
      <div class="dashboard-wrapper">
        <div class="dashboard-ecommerce">
          <!-- dashboard content -->
          <div class="container-fluid dashboard-content ">
            @yield('content')
          </div>
        </div>
      </div>
      <!-- footer -->
      <div class="footer align-bottom">
        <div class="container-fluid">
          <div class="row">
            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
              Copyright © 2018 fathanafif & Concept. All rights reserved. Dashboard by <a href="https://colorlib.com/wp/">Colorlib</a>.
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
              <div class="text-md-right footer-links d-none d-sm-block">
                <a href="javascript: void(0);">About</a>
                <a href="javascript: void(0);">Support</a>
                <a href="javascript: void(0);">Contact Us</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    @yield('scriptPartialCustomer')
  </body>
  </html>
  