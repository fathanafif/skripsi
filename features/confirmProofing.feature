Feature: Proofing Confirmation
    In order to merchant dapat mengetahui keputusan saya terkait kelanjutan order berdasarkan proofing yang telah dikirimkan
    As a Customer
    I want to memutuskan untuk melanjutkan atau membatalkan pesanan

Scenario: Confirm merchant's proofing
    Given I am on login page
    When I fill in "E-Mail Address" with "benny.vahlevi@gmail.com"
    And I fill in "Password" with "[cSt92/5Jj"
    And I press "Login"
    Then I should be on "explore"
    When I go to my poster order specification page
    Then I should see a "#proofing-image" element
    When I press "Process"
    Then I should be on "order/1"
    And I should see "Waiting for your payment" in the ".order-status" element
