Feature: Payment Slip Send
    In order to pemesanan yang saya lakukan wajib untuk dikerjakan pihak percetakan
    As a Customer
    I want to mengirim foto bukti pembayaran yang telah saya lakukan

Scenario: Send payment slip of my poster order to merchant
    Given I am on login page
    When I fill in "E-Mail Address" with "benny.vahlevi@gmail.com"
    And I fill in "Password" with "[cSt92/5Jj"
    And I press "Login"
    Then I should be on "explore"
    When I go to my poster order specification page
    Then I should see "Klik Document Solution"
    And I should see "9000008289299 a/n Adi Budiono"
    And I should see "Rp 200.000"
    Then I should see "Silahkan kirim bukti pembayaran"
    When I attach the file "assets/testing/paymentSlip.jpg" to "payment_slip"
    And I press "Send Payment Slip"
    Then I should be on "order/1"
    And I should see "Transfered" in the ".order-status" element
    And I should see "Menunggu merchant melakukan verifikasi pembayaran yang telah Anda lakukan."
