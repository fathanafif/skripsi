Feature: Order Finishing
    In order to customer saya dapat mengetahui bahwa status pesanannya sudah selesai dikerjakan
    As a Merchant
    I want to mengubah status pemesanan menjadi selesai dan dapat diambil customer atau dikirim ke lokasi customer

Scenario: Update order status to finish
    Given I am on login page
    When I fill in "E-Mail Address" with "klikdocumentsolution@gmail.com"
    And I fill in "Password" with "JvcG:(?om8"
    And I press "Login"
    Then I should be on "my_services" 
    When I go to benny vahlevi poster order specification page
    And I press "Done"
    Then should be on "order/1"
    And I should see "Order is ready for pick up" in the ".order-status" element