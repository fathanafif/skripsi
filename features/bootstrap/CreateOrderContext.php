<?php

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\TableNode;
use Behat\Gherkin\Node\PyStringNode;
use PHPUnit\Framework\Assert as PHPUnit;
use Behat\Behat\Hook\Scope\AfterStepScope;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Behat\Tester\Exception\PendingException;
#This will be needed if you require "behat/mink-selenium2-driver"
#use Behat\Mink\Driver\Selenium2Driver;

/**
* Defines application features from the specific context.
*/

class CreateOrderContext extends MinkContext implements Context, SnippetAcceptingContext
{
    

    /**
     * @Given I am on home page
     */
    public function iAmOnHomePage()
    {
        $this->visitPath('');
    }

    /**
     * @When I go to create order page
     */
    public function iGoToCreateOrderPage()
    {
        $this->visitPath('create_order/1');
    }

    /**
     * @Given I am on login page
     */
    public function iAmOnLoginPage()
    {
        $this->visitPath('login');
    }

    /**
     * @When I go to klik document solution poster service detail page
     */
    public function iGoToKlikDocumentSolutionPosterServiceDetailPage()
    {
        $this->visitPath('service/1');
    }

    /**
     * @When I go to klik document solution poster create order page
     */
    public function iGoToKlikDocumentSolutionPosterCreateOrderPage()
    {
        $this->visitPath('create_order/1');
    }

    /**
     * @Given I am on klik document solution poster create order page
     */
    public function iAmOnKlikDocumentSolutionPosterCreateOrderPage()
    {
        $this->visitPath('create_order/1');
    }

    /**
     * @Given I am on mangrove digital solution name card create order page
     */
    public function iAmOnMangroveDigitalSolutionNameCardCreateOrderPage()
    {
        $this->visitPath('create_order/2');
    }
}
