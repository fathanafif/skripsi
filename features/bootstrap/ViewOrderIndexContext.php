<?php

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\TableNode;
use Behat\Gherkin\Node\PyStringNode;
use PHPUnit\Framework\Assert as PHPUnit;
use Behat\Behat\Hook\Scope\AfterStepScope;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Behat\Tester\Exception\PendingException;
#This will be needed if you require "behat/mink-selenium2-driver"
#use Behat\Mink\Driver\Selenium2Driver;

/**
* Defines application features from the specific context.
*/

class ViewOrderIndexContext extends MinkContext implements Context, SnippetAcceptingContext
{
    

    /**
     * @Given I am on login page
     */
    public function iAmOnLoginPage()
    {
        $this->visitPath('login');
    }

    /**
     * @When I go to my orders page
     */
    public function iGoToMyOrdersPage()
    {
        $this->visitPath('order');
    }
}
