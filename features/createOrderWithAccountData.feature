Feature: Order Print Service
    In order to mencetak berkas sesuai apa yang saya butuhkan
    As a Customer
    I want to melakukan pemesanan jasa cetak menggunakan sistem Print Online

Scenario: Create order with login and completed user data
    Given I am on login page
    When I fill in "E-Mail Address" with "benny.vahlevi@gmail.com"
    And I fill in "Password" with "[cSt92/5Jj"
    And I press "Login"
    Then I should be on "explore"
    When I go to klik document solution poster service detail page
    Then I should be on "service/1"
    And I should see "Poster"
    And I should see "Klik Document Solution"
    When I go to klik document solution poster create order page
    Then I should be on "create_order/1"
    And I should see "Order Checkout" 
    And I should see "Poster" 
    And I should see "Klik Document Solution"

Scenario: Fill create order form using file upload to klik document solution
    Given I am on klik document solution poster create order page
    When I fill in "Catatan order" with "ini adalah catatan order poster saya"
    And I select "Art Carton 100gsm A3 (29.7 x 42 cm) Rp 4000/lembar" from "Bahan & ukuran"
    And I fill in "Jumlah rangkap" with "50"
    # And I attach the file "assets/testing/testPhotoshop.psd" to "file"
    And I select "Kirim ke lokasi saya" from "Pengambilan barang"
    And I check "checkBox"
    And I press "Order"
    Then I should be on "order"
    And I should see "Poster"
    And I should see "Klik Document Solution"
    And I should see "Art Carton"
    And I should see "Rp 200.000"
    And I should see "Pending" in the ".order-status" element
    And I should see "Menunggu konfirmasi pihak percetakan (merchant)."

Scenario: Fill create order form using file link to mangrove digital printing
    Given I am on mangrove digital solution name card create order page
    When I fill in "Catatan order" with "ini adalah catatan order kartu nama 2 sisi saya"
    And I select "Ivory 100gsm A3+ (32.9 x 48.3 cm) Rp 3500/lembar" from "Bahan & ukuran"
    And I fill in "Jumlah rangkap" with "50"
    And I fill in "fileLink" with "https://drive.google.com/open?id=1W5eyeTOuxNje0OKVZ2xZqNCgJA4VX_YV"
    And I select "Ambil ke lokasi percetakan" from "Pengambilan barang"
    And I check "checkBox"
    And I press "Order"
    Then I should be on "order"
    And I should see "Name Card"
    And I should see "mangrove digital printing"
    And I should see "Ivory"
    And I should see "Rp 200.000"
    And I should see "Pending" in the ".order-status" element
    And I should see "Menunggu konfirmasi pihak percetakan (merchant)."