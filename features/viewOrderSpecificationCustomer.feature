Feature: Order Specification
    In order to mengetahui rincian pesanan yang saya tujukan kepada percetakan
    As a Customer
    I want to melihat spesifikasi pesanan saya

Scenario: View my poster order to klik document solution and name card 2 sisi to mangrove digital printing
    Given I am on login page
    When I fill in "E-Mail Address" with "benny.vahlevi@gmail.com"
    And I fill in "Password" with "[cSt92/5Jj"
    And I press "Login"
    Then I should be on "explore"
    When I go to poster order specification page
    Then I should see "Poster"
    And I should see "Klik Document Solution"
    And I should see "Rp 200.000"
    And I should see a ".order-code" element
    And I should see a ".order-date" element
    And I should see a ".order-status" element
    And I should see a ".order-note" element
    When I go to name card order specification page
    Then I should see "Name Card 2 Sisi"
    And I should see "Mangrove Digital Printing"
    And I should see "Rp 175.000"
    And I should see a ".order-code" element
    And I should see a ".order-date" element
    And I should see a ".order-status" element
    And I should see a ".order-note" element