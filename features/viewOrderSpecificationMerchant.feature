Feature: Order Specification
    In order to mengetahui rincian pesanan yang ditujukan pelanggan kepada percetakan
    As a Merchant
    I want to melihat spesifikasi pesanan

Scenario: View benny vahlevi poster order specification
    Given I am on login page
    When I fill in "E-Mail Address" with "klikdocumentsolution@gmail.com"
    And I fill in "Password" with "JvcG:(?om8"
    And I press "Login"
    Then I should be on "my_services"
    When I go to poster order specification page
    Then I should see "Poster" in the "#serviceName" element
    And I should see an ".order-code" element
    And I should see an ".order-date" element
    And I should see an ".order-status" element
    And I should see "Benny Vahlevi" in the "#customerName" element
    And I should see "9000005436432" in the "#bankAccount" element
    And I should see "Mandiri" in the "#bankAccount" element
    And I should see "Benny Vahlevi" in the "#accountName" element
    And I should see "Rp 200.000" in the "#totalBiaya" element
    And I should see "Art Carton" in the "#materialName" element
    And I should see "A3 (29.7 x 42 cm) 100 gsm" in the "#materialSize" element
    And I should see "Rp 4.000/lembar" in the "#hargaSatuan" element
    And I should see "50 rangkap" in the "#copies" element
    And I should see "Kirim ke lokasi customer"