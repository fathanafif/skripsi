Feature: Order Index
    In order to mengetahui informasi seluruh pemesanan
    As a Customer
    I want to melihat seluruh pemesanan yang pernah saya lakukan

Scenario: View my order index 
    Given I am on login page
    When I fill in "E-Mail Address" with "benny.vahlevi@gmail.com"
    And I fill in "Password" with "[cSt92/5Jj"
    And I press "Login"
    Then I should be on "explore"
    When I go to my orders page
    Then I should see "Jumlah Order: 2"
    And I should see "Poster"
    And I should see "Klik Document Solution"
    And I should see "Art Carton"
    And I should see "A3 (29.7 x 42 cm) 100 gsm"
    And I should see "Rp 4.000/lembar"
    And I should see "50 rangkap"
    And I should see "Kirim ke lokasi saya"
    And I should see "Name Card 2 Sisi"
    And I should see "Mangrove Digital Printing"
    And I should see "Ivory"
    And I should see "A3+ (32.9 x 48.3 cm) 100 gsm"
    And I should see "Rp 3.500/lembar"
    And I should see "50 rangkap"
    And I should see "Ambil ke percetakan"
    And I should see an ".order-code" element
    And I should see an ".order-date" element
    And I should see an ".order-status" element