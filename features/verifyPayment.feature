Feature: Confirm Payment
    In order to customer dapat mengetahui bahwa pembayaran yang dilakukan sudah sesuai atau belum berdasarkan tagihan
    As a Merchant
    I want to memutuskan untuk menerima atau menolak pembayaran

Scenario: Verify customer's payment
    Given I am on login page
    When I fill in "E-Mail Address" with "klikdocumentsolution@gmail.com"
    And I fill in "Password" with "JvcG:(?om8"
    And I press "Login"
    Then I should be on "my_services"
    When I go to benny vahlevi poster order specification page
    Then I should see a "#payment-slip" element
    When I press "Process"
    Then I should be on "order/1"
    And I should see "Overall print is on going" in the ".order-status" element
