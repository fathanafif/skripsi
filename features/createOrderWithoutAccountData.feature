Feature: Order Print Service
    In order to mencetak poster di percetakan Klik Document Solution sesuai apa yang saya butuhkan
    As a Customer tanpa melengkapi data akun
    I want to melakukan pemesanan jasa cetak poster menggunakan sistem Print Online

Scenario: Create order without login
    Given I am on home page
    When I go to create order page
    Then I should be on "login"
    And I should see "Login"
    When I fill in "E-Mail Address" with "fathanafif@gmail.com"
    And I fill in "Password" with "8h:1-F2kbx"
    And I press "Login"
    Then I should be on "account/edit"