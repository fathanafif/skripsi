Feature: Order Index
    In order to mengetahui informasi seluruh pemesanan
    As a Merchant
    I want to melihat seluruh pemesanan yang ditujukan pelanggan kepada saya

Scenario: View my order index
    Given I am on login page
    When I fill in "E-Mail Address" with "klikdocumentsolution@gmail.com"
    And I fill in "Password" with "JvcG:(?om8"
    And I press "Login"
    Then I should be on "my_services"
    When I go to my orders page
    Then I should see "Jumlah Order: 1"
    And I should see "Poster"
    And I should see "Benny Vahlevi"
    And I should see "Art Carton"
    And I should see "A3 (29.7 x 42 cm) 100 gsm"
    And I should see "Rp 4.000/lembar"
    And I should see "50 rangkap"
    And I should see "Rp 200.000"
    And I should see "Kirim ke lokasi customer"
    And I should see an ".order-code" element
    And I should see an ".order-date" element
    And I should see an ".order-status" element