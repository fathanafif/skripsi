Feature: Proofing Photo Send
    In order to pelanggan saya dapat melihat hasil sampel cetak order
    As a Merchant
    I want to mengirim foto sampel hasil cetak 

Scenario: Send proofing photo to benny vahlevi poster order
    Given I am on login page
    When I fill in "E-Mail Address" with "klikdocumentsolution@gmail.com"
    And I fill in "Password" with "JvcG:(?om8"
    And I press "Login"
    Then I should be on "my_services"
    When I go to benny vahlevi poster order specification page
    And I attach the file "assets/testing/daqu.jpg" to "proofing_photo"
    And I press "Send Proofing"
    Then I should be on "order/1"
    And I should see "Proofing is sent to customer" in the ".order-status" element