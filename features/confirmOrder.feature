Feature: Confirm Order
    In order to pelanggan saya dapat mengetahui konfirmasi pemesanan
    As a Merchant
    I want to menerima order pelanggan yang ditujukan kepada saya

Scenario: Confirm benny vahlevi's poster order
    Given I am on login page
    When I fill in "E-Mail Address" with "klikdocumentsolution@gmail.com"
    And I fill in "Password" with "JvcG:(?om8"
    And I press "Login"
    Then I should be on "my_services"
    When I go to benny vahlevi poster order specification page
    And I fill in "Message for customer" with "Terima kasih, order Anda akan kami kerjakan."
    And I press "Accept Order"
    Then I should be on "order/1"
    And I should see "Confirmed by merchant" in the ".order-status" element
    And I should see "Silahkan unggah foto hasil proofing Anda"
    And I should see a ".send-proofing" element